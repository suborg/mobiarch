window.LoadEngine = function(engineCfg, titleElem, canvasElem, gameData, gameTitle, gameFile) {
  var baseName = gameTitle.split('/').pop()
  var quirkInfo = baseName.split('.')[1], lsq = false, stq = false
  if(quirkInfo.indexOf('l')>-1) lsq = true
  if(quirkInfo.indexOf('s')>-1) stq = true

  var progData = {
    name: baseName.split('.')[0],
    quirks: {
      lsq: lsq,
      stq: stq
    },
    data: gameData
  }, emuParams = {
    canvas: canvasElem,
    clockFactor: 20,
    beepShape: 'triangle',
    titleElement: titleElem,
    bgColor: '0, 24, 10',
    fgColor: '0, 255, 13',
    keyMapping: engineCfg.keys
  }, emuHandle = null

  //load the engine script
  var s = document.createElement('script')
  s.src = window.engineRootDir + '/dale8.js'
  s.onload = function() {
    emuHandle = Dale(window, emuParams).run(progData)
  }
  document.body.appendChild(s)
}

