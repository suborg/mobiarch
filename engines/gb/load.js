window.LoadEngine = function(engineCfg, titleElem, canvasElem, gameData, gameTitle, gameFile) {
  var baseName = gameTitle.split('/').pop(), romBuffer = '', i, l = gameData.length
  var mapping = {}
  for(var k in engineCfg.keys) {
    if(!k.startsWith('sup_'))
      mapping[engineCfg.keys[k]] = k.toLowerCase()
  }
  for(i=0;i<l;i++)
    romBuffer += String.fromCharCode(gameData[i])
  titleElem.textContent = baseName
  //load the engine script
  var s = document.createElement('script')
  s.src = window.engineRootDir + '/jsgb-opti.js'
  s.onload = function() {
    var machinePaused = true
    //setup keys
    window.addEventListener('keydown', function(e) {
      if(e.key in mapping) {
        e.preventDefault()
        GameBoyKeyDown(mapping[e.key])
      }
      else if(e.key === engineCfg.keys.sup_Pause) {
        if(!machinePaused) {
          machinePaused = true
          for(var key in mapping)
            GameBoyKeyUp(mapping[key])
          pause()
        }
        else {
          machinePaused = false
          run()
        }
      }
    })
    window.addEventListener('keyup', function(e) {
      if(e.key in mapping) {
        e.preventDefault()
        GameBoyKeyUp(mapping[e.key])
      }
    })
    //start the machine
    var soundOn = window.confirm('Run with sound?')
    window.start(canvasElem, romBuffer, soundOn)
    machinePaused = false
    titleElem.textContent = gameboy.name
    canvasElem.style.height = Math.round(canvasElem.width * engineCfg.screen.height / engineCfg.screen.width)+'px'
  }
  document.body.appendChild(s)
}

