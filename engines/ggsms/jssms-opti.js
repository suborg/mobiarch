//JSSMS engine by gmarty, optimized for KaiOS 2.5.x by Luxferre 

(function(window) {
	var DEBUG = !1,
		ACCURATE = !0,
		LITTLE_ENDIAN = !0,
		REFRESH_EMULATION = !0,
		LIGHTGUN = !1,
		VDP_SPRITE_COLLISIONS = ACCURATE,
		PAGE_SIZE = 16384,
		CLOCK_NTSC = 3579545,
		CLOCK_PAL = 3546893;

	function JSSMS(a) {
		this.opts = {
			ui: JSSMS.DummyUI
		};
		if (void 0 !== a)
			for (var b in this.opts) void 0 !== a[b] && (this.opts[b] = a[b]);
		void 0 !== a.DEBUG && (DEBUG = a.DEBUG);
		this.keyboard = new JSSMS.Keyboard(this);
		this.ui = new this.opts.ui(this);
		this.vdp = new JSSMS.Vdp(this);
		this.psg = new JSSMS.SN76489(this);
		this.ports = new JSSMS.Ports(this);
		this.cpu = new JSSMS.Z80(this);
		this.ui.updateStatus("Ready to load a ROM.");
		this.ui = this.ui
	}
	JSSMS.prototype = {
		isRunning: !1,
		cyclesPerLine: 0,
		no_of_scanlines: 0,
		frameSkip: 0,
		fps: 0,
		frameskip_counter: 0,
		pause_button: !1,
		is_sms: !0,
		is_gg: !1,
		audioContext: null,
		audioBufferOffset: 0,
		emuWidth: 0,
		emuHeight: 0,
		z80Time: 0,
		drawTime: 0,
		z80TimeCounter: 0,
		drawTimeCounter: 0,
		frameCount: 0,
		romData: "",
		romFileName: "",
		lineno: 0,
		reset: function() {
			this.setVideoTiming(this.vdp.videoMode);
			this.frameCount = 0;
			this.frameskip_counter = this.frameSkip;
			this.keyboard.reset();
			this.ui.reset();
			this.vdp.reset();
			this.ports.reset();
			this.cpu.reset();
			this.psg.reset();
		},
		start: function() {
			var a = this;
			this.isRunning || (this.isRunning = !0, window.requestAnimationFrame(this.frame.bind(this)));
			this.ui.updateStatus("Running")
		},
		stop: function() {
			this.isRunning = !1
		},
		frame: function() {
			this.isRunning && (this.cpu.frame(), window.requestAnimationFrame(this.frame.bind(this)))
		},
		nextStep: function() {
			this.cpu.frame()
		},
		setSMS: function() {
			this.is_sms = !0;
			this.is_gg = !1;
			this.vdp.h_start = 0;
			this.vdp.h_end = 32;
			this.emuWidth = SMS_WIDTH;
			this.emuHeight = SMS_HEIGHT
		},
		setGG: function() {
			this.is_gg = !0;
			this.is_sms = !1;
			this.vdp.h_start = 6;
			this.vdp.h_end = 26;
			this.emuWidth = GG_WIDTH;
			this.emuHeight = GG_HEIGHT
		},
		setVideoTiming: function(a) {
			var b;
			a === NTSC || this.is_gg ?
				(this.fps = 60, this.no_of_scanlines = SMS_Y_PIXELS_NTSC, b = CLOCK_NTSC) : (this.fps = 50, this.no_of_scanlines = SMS_Y_PIXELS_PAL, b = CLOCK_PAL);
			this.cyclesPerLine = Math.round(b / this.fps / this.no_of_scanlines + 1);
			this.vdp.videoMode = a;
			this.psg.init(b)
		},
		readRomDirectly: function(a, b) {
			var d = JSSMS.Utils.getFileExtension(b);
			var f = a.length;
			"gg" === d ? this.setGG() : this.setSMS();
			d = this.loadROM(a, f);
			if (null === d) return !1;
			this.cpu.resetMemory(d);
			this.romData = a;
			this.romFileName = b;
			return !0
		},
		loadROM: function(a, b) {
			var d, f = Math.ceil(b / PAGE_SIZE), c = Array(f);
			for (b = 0; b < f; b++) {
				c[b] = JSSMS.Utils.Array(PAGE_SIZE)
				for (d = 0; d < PAGE_SIZE; d++) c[b].setUint8(d, a.charCodeAt(b * PAGE_SIZE + d));
      }
			return c
		},
		reloadRom: function() {
			return "" !== this.romData && "" !== this.romFileName ? this.readRomDirectly(this.romData, this.romFileName) : !1
		}
	};

	JSSMS.Utils = {
		rndInt: function(a) {
			return Math.round(Math.random() * a)
		},
		Array: function(a) {
			return new DataView(new ArrayBuffer(a))
		},
		copyArrayElements: function(a, b, d, f, c) {
			for(;c--;) d.setInt8(f + c, a.getInt8(b + c))
		},
		console: {
			log: function() {
				return DEBUG ?
					window.console.log.bind(window.console) : function(a) {}
			}(),
			error: function() {
				return DEBUG ? window.console.error.bind(window.console) : function(a) {}
			}(),
			time: function() {
				return DEBUG && window.console.time ? window.console.time.bind(window.console) : function(a) {}
			}(),
			timeEnd: function() {
				return DEBUG && window.console.timeEnd ? window.console.timeEnd.bind(window.console) : function(a) {}
			}()
		},
		toHex: function(a) {
			var b = 0 > a;
			a = Math.abs(a).toString(16).toUpperCase();
			a.length % 2 && (a = "0" + a);
			return b ? "-0x" + a : "0x" + a
		},
		getPrefix: function(a, b) {
			var d = !1;
			void 0 === b && (b = document);
			a.some(function(a) {
				return a in b ? (d = a, !0) : !1
			});
			return d
		},
		getFileExtension: function(a) {
			return "string" !== typeof a ? "" : a.split(".").pop().toLowerCase()
		},
		getFileName: function(a) {
			if ("string" !== typeof a) return "";
			a = a.split(".");
			a.pop();
			return a.join(".").split("/").pop()
		},
		crc32: function(a) {
			var b = function() {
				for (var a, b = new Uint32Array(256), c = 0; 256 > c; c++) {
					a = c;
					for (var g = 0; 8 > g; g++) a = a & 1 ? 3988292384 ^ a >>> 1 : a >>> 1;
					b[c] = a
				}
				return b
			}();
			this.crc32 = function(a) {
				for (var f = -1, c = 0; c < a.length; c++) f = f >>> 8 ^ b[(f ^ a.charCodeAt(c)) & 255];
				return (f ^ -1) >>> 0
			};
			return this.crc32(a)
		}
	};
	var LOG_LENGTH = 100;

	var HALT_SPEEDUP = !0,
		F_CARRY = 1,
		F_NEGATIVE = 2,
		F_PARITY = 4,
		F_OVERFLOW = 4,
		F_BIT3 = 8,
		F_HALFCARRY = 16,
		F_BIT5 = 32,
		F_ZERO = 64,
		F_SIGN = 128,
		BIT_0 = 1,
		BIT_1 = 2,
		BIT_2 = 4,
		BIT_3 = 8,
		BIT_4 = 16,
		BIT_5 = 32,
		BIT_6 = 64,
		BIT_7 = 128,
		OP_STATES = new Uint8Array([4, 10, 7, 6, 4, 4, 7, 4, 4, 11, 7, 6, 4, 4, 7, 4, 8, 10, 7, 6, 4, 4, 7, 4, 12, 11, 7, 6, 4, 4, 7, 4, 7, 10, 16, 6, 4, 4, 7, 4, 7, 11, 16, 6, 4, 4, 7, 4, 7, 10, 13, 6, 11, 11, 10, 4, 7, 11, 13, 6, 4, 4, 7, 4, 4, 4, 4, 4, 4, 4, 7, 4, 4, 4, 4, 4, 4, 4, 7, 4, 4, 4, 4, 4, 4, 4, 7, 4, 4, 4, 4, 4, 4, 4, 7, 4, 4, 4, 4, 4, 4, 4, 7, 4, 4, 4, 4, 4, 4, 4, 7, 4, 7, 7, 7, 7, 7, 7, 4, 7, 4, 4, 4, 4, 4, 4, 7, 4,
			4, 4, 4, 4, 4, 4, 7, 4, 4, 4, 4, 4, 4, 4, 7, 4, 4, 4, 4, 4, 4, 4, 7, 4, 4, 4, 4, 4, 4, 4, 7, 4, 4, 4, 4, 4, 4, 4, 7, 4, 4, 4, 4, 4, 4, 4, 7, 4, 4, 4, 4, 4, 4, 4, 7, 4, 4, 4, 4, 4, 4, 4, 7, 4, 5, 10, 10, 10, 10, 11, 7, 11, 5, 10, 10, 0, 10, 17, 7, 11, 5, 10, 10, 11, 10, 11, 7, 11, 5, 4, 10, 11, 10, 0, 7, 11, 5, 10, 10, 19, 10, 11, 7, 11, 5, 4, 10, 4, 10, 0, 7, 11, 5, 10, 10, 4, 10, 11, 7, 11, 5, 6, 10, 4, 10, 0, 7, 11
		]),
		OP_CB_STATES = new Uint8Array([8, 8, 8, 8, 8, 8, 15, 8, 8, 8, 8, 8, 8, 8, 15, 8, 8, 8, 8, 8, 8, 8, 15, 8, 8, 8, 8, 8, 8, 8, 15, 8, 8, 8, 8, 8, 8, 8, 15, 8, 8, 8, 8, 8, 8, 8, 15, 8, 8, 8, 8, 8, 8, 8, 15, 8, 8, 8, 8, 8, 8, 8, 15, 8, 8, 8, 8, 8, 8, 8, 12, 8, 8, 8, 8, 8, 8, 8,
			12, 8, 8, 8, 8, 8, 8, 8, 12, 8, 8, 8, 8, 8, 8, 8, 12, 8, 8, 8, 8, 8, 8, 8, 12, 8, 8, 8, 8, 8, 8, 8, 12, 8, 8, 8, 8, 8, 8, 8, 12, 8, 8, 8, 8, 8, 8, 8, 12, 8, 8, 8, 8, 8, 8, 8, 15, 8, 8, 8, 8, 8, 8, 8, 15, 8, 8, 8, 8, 8, 8, 8, 15, 8, 8, 8, 8, 8, 8, 8, 15, 8, 8, 8, 8, 8, 8, 8, 15, 8, 8, 8, 8, 8, 8, 8, 15, 8, 8, 8, 8, 8, 8, 8, 15, 8, 8, 8, 8, 8, 8, 8, 15, 8, 8, 8, 8, 8, 8, 8, 15, 8, 8, 8, 8, 8, 8, 8, 15, 8, 8, 8, 8, 8, 8, 8, 15, 8, 8, 8, 8, 8, 8, 8, 15, 8, 8, 8, 8, 8, 8, 8, 15, 8, 8, 8, 8, 8, 8, 8, 15, 8, 8, 8, 8, 8, 8, 8, 15, 8, 8, 8, 8, 8, 8, 8, 15, 8
		]),
		OP_DD_STATES = new Uint8Array([4, 4, 4, 4, 4, 4, 4, 4, 4, 15, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 15, 4, 4, 4, 4, 4, 4, 4, 14, 20, 10, 8,
			8, 11, 4, 4, 15, 20, 10, 8, 8, 11, 4, 4, 4, 4, 4, 23, 23, 19, 4, 4, 15, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 8, 8, 19, 4, 4, 4, 4, 4, 8, 8, 19, 4, 4, 4, 4, 4, 8, 8, 19, 4, 4, 4, 4, 4, 8, 8, 19, 4, 8, 8, 8, 8, 8, 8, 19, 8, 8, 8, 8, 8, 8, 8, 19, 8, 19, 19, 19, 19, 19, 19, 4, 19, 4, 4, 4, 4, 8, 8, 19, 4, 4, 4, 4, 4, 8, 8, 19, 4, 4, 4, 4, 4, 8, 8, 19, 4, 4, 4, 4, 4, 8, 8, 19, 4, 4, 4, 4, 4, 8, 8, 19, 4, 4, 4, 4, 4, 8, 8, 19, 4, 4, 4, 4, 4, 8, 8, 19, 4, 4, 4, 4, 4, 8, 8, 19, 4, 4, 4, 4, 4, 8, 8, 19, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 0, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 14, 4, 23, 4, 15, 4, 4, 4, 8, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 10, 4, 4, 4, 4, 4, 4
		]),
		OP_INDEX_CB_STATES = new Uint8Array([23,
			23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23,
			23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23
		]),
		OP_ED_STATES = new Uint8Array([8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 12, 12, 15, 20, 8, 14, 8, 9, 12, 12, 15, 20, 8, 14, 8, 9, 12, 12, 15, 20, 8, 14, 8, 9, 12,
			12, 15, 20, 8, 14, 8, 9, 12, 12, 15, 20, 8, 14, 8, 18, 12, 12, 15, 20, 8, 14, 8, 18, 8, 12, 15, 20, 8, 14, 8, 8, 12, 12, 15, 20, 8, 14, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 16, 16, 16, 16, 8, 8, 8, 8, 16, 16, 16, 16, 8, 8, 8, 8, 16, 16, 16, 16, 8, 8, 8, 8, 16, 16, 16, 16, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8
		]);

	JSSMS.Z80 = function(a) {
		this.main = a;
		this.vdp = a.vdp;
		this.psg = a.psg;
		this.port = a.ports;
		this.im = this.sp = this.pc = 0;
		this.interruptLine = this.EI_inst = this.halt = this.iff2 = this.iff1 = !1;
		this.tstates = this.totalCycles = this.f2 = this.f = this.i = this.r = this.iyH = this.iyL = this.ixH = this.ixL = this.l2 = this.h2 = this.l = this.h = this.e2 = this.d2 = this.e = this.d = this.c2 = this.b2 = this.c = this.b = this.a2 = this.a = this.interruptVector = 0;
		this.rom = [];
		this.sram = JSSMS.Utils.Array(32768);
		this.useSRAM = !1;
		this.frameReg = Array(4);
		this.number_of_pages = this.romPageMask = 0;
		this.memWriteMap = JSSMS.Utils.Array(8192);
		this.DAA_TABLE = new Uint16Array(2048);
		this.SZ_TABLE = new Uint8Array(256);
		this.SZP_TABLE = new Uint8Array(256);
		this.SZHV_INC_TABLE = new Uint8Array(256);
		this.SZHV_DEC_TABLE = new Uint8Array(256);
		this.SZHVC_ADD_TABLE = new Uint8Array(131072);
		this.SZHVC_SUB_TABLE = new Uint8Array(131072);
		this.SZ_BIT_TABLE = new Uint8Array(256);
		this.generateFlagTables();
		this.generateDAATable();
		this.generateMemory()
	};
	JSSMS.Z80.prototype = {
		reset: function() {
			this.pc = this.f2 = this.f = this.i = this.r = this.iyL = this.iyH = this.ixL = this.ixH = this.h = this.l = this.h2 = this.l2 = this.d = this.e = this.d2 = this.e2 = this.b = this.c = this.b2 = this.c2 = this.a = this.a2 = 0;
			this.sp = 57328;
			this.im = this.tstates = this.totalCycles = 0;
			this.EI_inst = this.iff2 = this.iff1 = !1;
			this.interruptVector = 0;
			this.halt = !1;
		},
		frame: function() {
			this.lineno = 0;
			this.tstates += this.main.cyclesPerLine;
			this.totalCycles = this.main.cyclesPerLine;
			for(;!(this.interpret(), 0 >= this.tstates && this.eol()););
		},
		eol: function() {
			this.vdp.line = this.lineno;
			192 > this.lineno && this.vdp.drawLine(this.lineno);
			this.vdp.interrupts(this.lineno);
			this.interruptLine && this.interrupt();
			this.lineno++;
			if (this.lineno >= this.main.no_of_scanlines) return this.eof(), !0;
			this.tstates += this.main.cyclesPerLine;
			this.totalCycles = this.main.cyclesPerLine;
			return !1
		},
		eof: function() {
			this.main.pause_button && (this.nmi(), this.main.pause_button = !1);
			this.main.ui.writeFrame()
		},
		branches: [Object.create(null), Object.create(null), Object.create(null)],
		interpret: function() {
			var a;
			a = this.getUint8(this.pc++);
			this.tstates -= OP_STATES[a];
			REFRESH_EMULATION && this.incR();
			switch (a) {
				case 1:
					this.setBC(this.getUint16(this.pc++));
					this.pc++;
					break;
				case 2:
					this.setUint8(this.getBC(),
						this.a);
					break;
				case 3:
					this.incBC();
					break;
				case 4:
					this.b = this.inc8(this.b);
					break;
				case 5:
					this.b = this.dec8(this.b);
					break;
				case 6:
					this.b = this.getUint8(this.pc++);
					break;
				case 7:
					this.rlca_a();
					break;
				case 8:
					this.exAF();
					break;
				case 9:
					this.setHL(this.add16(this.getHL(), this.getBC()));
					break;
				case 10:
					this.a = this.getUint8(this.getBC());
					break;
				case 11:
					this.decBC();
					break;
				case 12:
					this.c = this.inc8(this.c);
					break;
				case 13:
					this.c = this.dec8(this.c);
					break;
				case 14:
					this.c = this.getUint8(this.pc++);
					break;
				case 15:
					this.rrca_a();
					break;
				case 16:
					this.b = this.b - 1 & 255;
					this.jr(0 !== this.b);
					break;
				case 17:
					this.setDE(this.getUint16(this.pc++));
					this.pc++;
					break;
				case 18:
					this.setUint8(this.getDE(), this.a);
					break;
				case 19:
					this.incDE();
					break;
				case 20:
					this.d = this.inc8(this.d);
					break;
				case 21:
					this.d = this.dec8(this.d);
					break;
				case 22:
					this.d = this.getUint8(this.pc++);
					break;
				case 23:
					this.rla_a();
					break;
				case 24:
					this.pc += this.getInt8(this.pc);
					break;
				case 25:
					this.setHL(this.add16(this.getHL(), this.getDE()));
					break;
				case 26:
					this.a = this.getUint8(this.getDE());
					break;
				case 27:
					this.decDE();
					break;
				case 28:
					this.e = this.inc8(this.e);
					break;
				case 29:
					this.e = this.dec8(this.e);
					break;
				case 30:
					this.e = this.getUint8(this.pc++);
					break;
				case 31:
					this.rra_a();
					break;
				case 32:
					this.jr(0 === (this.f & F_ZERO));
					break;
				case 33:
					this.setHL(this.getUint16(this.pc++));
					this.pc++;
					break;
				case 34:
					this.setUint16(this.getUint16(this.pc++), this.getHL());
					this.pc++;
					break;
				case 35:
					this.incHL();
					break;
				case 36:
					this.h = this.inc8(this.h);
					break;
				case 37:
					this.h = this.dec8(this.h);
					break;
				case 38:
					this.h = this.getUint8(this.pc++);
					break;
				case 39:
					this.daa();
					break;
				case 40:
					this.jr(0 !== (this.f & F_ZERO));
					break;
				case 41:
					this.setHL(this.add16(this.getHL(), this.getHL()));
					break;
				case 42:
					this.setHL(this.getUint16(this.getUint16(this.pc++)));
					this.pc++;
					break;
				case 43:
					this.decHL();
					break;
				case 44:
					this.l = this.inc8(this.l);
					break;
				case 45:
					this.l = this.dec8(this.l);
					break;
				case 46:
					this.l = this.getUint8(this.pc++);
					break;
				case 47:
					this.cpl_a();
					break;
				case 48:
					this.jr(0 === (this.f & F_CARRY));
					break;
				case 49:
					this.sp = this.getUint16(this.pc++);
					this.pc++;
					break;
				case 50:
					this.setUint8(this.getUint16(this.pc++),
						this.a);
					this.pc++;
					break;
				case 51:
					this.sp++;
					break;
				case 52:
					this.incMem(this.getHL());
					break;
				case 53:
					this.decMem(this.getHL());
					break;
				case 54:
					this.setUint8(this.getHL(), this.getUint8(this.pc++));
					break;
				case 55:
					this.f |= F_CARRY;
					this.f &= ~F_NEGATIVE;
					this.f &= ~F_HALFCARRY;
					break;
				case 56:
					this.jr(0 !== (this.f & F_CARRY));
					break;
				case 57:
					this.setHL(this.add16(this.getHL(), this.sp));
					break;
				case 58:
					this.a = this.getUint8(this.getUint16(this.pc++));
					this.pc++;
					break;
				case 59:
					this.sp--;
					break;
				case 60:
					this.a = this.inc8(this.a);
					break;
				case 61:
					this.a = this.dec8(this.a);
					break;
				case 62:
					this.a = this.getUint8(this.pc++);
					break;
				case 63:
					this.ccf();
					break;
				case 65:
					this.b = this.c;
					break;
				case 66:
					this.b = this.d;
					break;
				case 67:
					this.b = this.e;
					break;
				case 68:
					this.b = this.h;
					break;
				case 69:
					this.b = this.l;
					break;
				case 70:
					this.b = this.getUint8(this.getHL());
					break;
				case 71:
					this.b = this.a;
					break;
				case 72:
					this.c = this.b;
					break;
				case 74:
					this.c = this.d;
					break;
				case 75:
					this.c = this.e;
					break;
				case 76:
					this.c = this.h;
					break;
				case 77:
					this.c = this.l;
					break;
				case 78:
					this.c = this.getUint8(this.getHL());
					break;
				case 79:
					this.c = this.a;
					break;
				case 80:
					this.d = this.b;
					break;
				case 81:
					this.d = this.c;
					break;
				case 83:
					this.d = this.e;
					break;
				case 84:
					this.d = this.h;
					break;
				case 85:
					this.d = this.l;
					break;
				case 86:
					this.d = this.getUint8(this.getHL());
					break;
				case 87:
					this.d = this.a;
					break;
				case 88:
					this.e = this.b;
					break;
				case 89:
					this.e = this.c;
					break;
				case 90:
					this.e = this.d;
					break;
				case 92:
					this.e = this.h;
					break;
				case 93:
					this.e = this.l;
					break;
				case 94:
					this.e = this.getUint8(this.getHL());
					break;
				case 95:
					this.e = this.a;
					break;
				case 96:
					this.h = this.b;
					break;
				case 97:
					this.h =
						this.c;
					break;
				case 98:
					this.h = this.d;
					break;
				case 99:
					this.h = this.e;
					break;
				case 101:
					this.h = this.l;
					break;
				case 102:
					this.h = this.getUint8(this.getHL());
					break;
				case 103:
					this.h = this.a;
					break;
				case 104:
					this.l = this.b;
					break;
				case 105:
					this.l = this.c;
					break;
				case 106:
					this.l = this.d;
					break;
				case 107:
					this.l = this.e;
					break;
				case 108:
					this.l = this.h;
					break;
				case 110:
					this.l = this.getUint8(this.getHL());
					break;
				case 111:
					this.l = this.a;
					break;
				case 112:
					this.setUint8(this.getHL(), this.b);
					break;
				case 113:
					this.setUint8(this.getHL(), this.c);
					break;
				case 114:
					this.setUint8(this.getHL(), this.d);
					break;
				case 115:
					this.setUint8(this.getHL(), this.e);
					break;
				case 116:
					this.setUint8(this.getHL(), this.h);
					break;
				case 117:
					this.setUint8(this.getHL(), this.l);
					break;
				case 118:
					HALT_SPEEDUP && (this.tstates = 0);
					this.halt = !0;
					this.pc--;
					break;
				case 119:
					this.setUint8(this.getHL(), this.a);
					break;
				case 120:
					this.a = this.b;
					break;
				case 121:
					this.a = this.c;
					break;
				case 122:
					this.a = this.d;
					break;
				case 123:
					this.a = this.e;
					break;
				case 124:
					this.a = this.h;
					break;
				case 125:
					this.a = this.l;
					break;
				case 126:
					this.a =
						this.getUint8(this.getHL());
					break;
				case 128:
					this.add_a(this.b);
					break;
				case 129:
					this.add_a(this.c);
					break;
				case 130:
					this.add_a(this.d);
					break;
				case 131:
					this.add_a(this.e);
					break;
				case 132:
					this.add_a(this.h);
					break;
				case 133:
					this.add_a(this.l);
					break;
				case 134:
					this.add_a(this.getUint8(this.getHL()));
					break;
				case 135:
					this.add_a(this.a);
					break;
				case 136:
					this.adc_a(this.b);
					break;
				case 137:
					this.adc_a(this.c);
					break;
				case 138:
					this.adc_a(this.d);
					break;
				case 139:
					this.adc_a(this.e);
					break;
				case 140:
					this.adc_a(this.h);
					break;
				case 141:
					this.adc_a(this.l);
					break;
				case 142:
					this.adc_a(this.getUint8(this.getHL()));
					break;
				case 143:
					this.adc_a(this.a);
					break;
				case 144:
					this.sub_a(this.b);
					break;
				case 145:
					this.sub_a(this.c);
					break;
				case 146:
					this.sub_a(this.d);
					break;
				case 147:
					this.sub_a(this.e);
					break;
				case 148:
					this.sub_a(this.h);
					break;
				case 149:
					this.sub_a(this.l);
					break;
				case 150:
					this.sub_a(this.getUint8(this.getHL()));
					break;
				case 151:
					this.sub_a(this.a);
					break;
				case 152:
					this.sbc_a(this.b);
					break;
				case 153:
					this.sbc_a(this.c);
					break;
				case 154:
					this.sbc_a(this.d);
					break;
				case 155:
					this.sbc_a(this.e);
					break;
				case 156:
					this.sbc_a(this.h);
					break;
				case 157:
					this.sbc_a(this.l);
					break;
				case 158:
					this.sbc_a(this.getUint8(this.getHL()));
					break;
				case 159:
					this.sbc_a(this.a);
					break;
				case 160:
					this.f = this.SZP_TABLE[this.a &= this.b] | F_HALFCARRY;
					break;
				case 161:
					this.f = this.SZP_TABLE[this.a &= this.c] | F_HALFCARRY;
					break;
				case 162:
					this.f = this.SZP_TABLE[this.a &= this.d] | F_HALFCARRY;
					break;
				case 163:
					this.f = this.SZP_TABLE[this.a &= this.e] | F_HALFCARRY;
					break;
				case 164:
					this.f = this.SZP_TABLE[this.a &= this.h] | F_HALFCARRY;
					break;
				case 165:
					this.f =
						this.SZP_TABLE[this.a &= this.l] | F_HALFCARRY;
					break;
				case 166:
					this.f = this.SZP_TABLE[this.a &= this.getUint8(this.getHL())] | F_HALFCARRY;
					break;
				case 167:
					this.f = this.SZP_TABLE[this.a] | F_HALFCARRY;
					break;
				case 168:
					this.f = this.SZP_TABLE[this.a ^= this.b];
					break;
				case 169:
					this.f = this.SZP_TABLE[this.a ^= this.c];
					break;
				case 170:
					this.f = this.SZP_TABLE[this.a ^= this.d];
					break;
				case 171:
					this.f = this.SZP_TABLE[this.a ^= this.e];
					break;
				case 172:
					this.f = this.SZP_TABLE[this.a ^= this.h];
					break;
				case 173:
					this.f = this.SZP_TABLE[this.a ^= this.l];
					break;
				case 174:
					this.f = this.SZP_TABLE[this.a ^= this.getUint8(this.getHL())];
					break;
				case 175:
					this.f = this.SZP_TABLE[this.a = 0];
					break;
				case 176:
					this.f = this.SZP_TABLE[this.a |= this.b];
					break;
				case 177:
					this.f = this.SZP_TABLE[this.a |= this.c];
					break;
				case 178:
					this.f = this.SZP_TABLE[this.a |= this.d];
					break;
				case 179:
					this.f = this.SZP_TABLE[this.a |= this.e];
					break;
				case 180:
					this.f = this.SZP_TABLE[this.a |= this.h];
					break;
				case 181:
					this.f = this.SZP_TABLE[this.a |= this.l];
					break;
				case 182:
					this.f = this.SZP_TABLE[this.a |= this.getUint8(this.getHL())];
					break;
				case 183:
					this.f = this.SZP_TABLE[this.a];
					break;
				case 184:
					this.cp_a(this.b);
					break;
				case 185:
					this.cp_a(this.c);
					break;
				case 186:
					this.cp_a(this.d);
					break;
				case 187:
					this.cp_a(this.e);
					break;
				case 188:
					this.cp_a(this.h);
					break;
				case 189:
					this.cp_a(this.l);
					break;
				case 190:
					this.cp_a(this.getUint8(this.getHL()));
					break;
				case 191:
					this.cp_a(this.a);
					break;
				case 192:
					this.ret(0 === (this.f & F_ZERO));
					break;
				case 193:
					this.setBC(this.getUint16(this.sp));
					this.sp += 2;
					break;
				case 194:
					this.jp(0 === (this.f & F_ZERO));
					break;
				case 195:
					this.pc =
						this.getUint16(this.pc);
					break;
				case 196:
					this.call(0 === (this.f & F_ZERO));
					break;
				case 197:
					this.push(this.getBC());
					break;
				case 198:
					this.add_a(this.getUint8(this.pc++));
					break;
				case 199:
					this.push(this.pc);
					this.pc = 0;
					break;
				case 200:
					this.ret(0 !== (this.f & F_ZERO));
					break;
				case 201:
					this.pc = this.getUint16(this.sp);
					this.sp += 2;
					break;
				case 202:
					this.jp(0 !== (this.f & F_ZERO));
					break;
				case 203:
					this.doCB(this.getUint8(this.pc++));
					break;
				case 204:
					this.call(0 !== (this.f & F_ZERO));
					break;
				case 205:
					this.push(this.pc + 2);
					this.pc = this.getUint16(this.pc);
					break;
				case 206:
					this.adc_a(this.getUint8(this.pc++));
					break;
				case 207:
					this.push(this.pc);
					this.pc = 8;
					break;
				case 208:
					this.ret(0 === (this.f & F_CARRY));
					break;
				case 209:
					this.setDE(this.getUint16(this.sp));
					this.sp += 2;
					break;
				case 210:
					this.jp(0 === (this.f & F_CARRY));
					break;
				case 211:
					this.port.out(this.getUint8(this.pc++), this.a);
					break;
				case 212:
					this.call(0 === (this.f & F_CARRY));
					break;
				case 213:
					this.push(this.getDE());
					break;
				case 214:
					this.sub_a(this.getUint8(this.pc++));
					break;
				case 215:
					this.push(this.pc);
					this.pc = 16;
					break;
				case 216:
					this.ret(0 !==
						(this.f & F_CARRY));
					break;
				case 217:
					this.exBC();
					this.exDE();
					this.exHL();
					break;
				case 218:
					this.jp(0 !== (this.f & F_CARRY));
					break;
				case 219:
					this.a = this.port.in_(this.getUint8(this.pc++));
					break;
				case 220:
					this.call(0 !== (this.f & F_CARRY));
					break;
				case 221:
					this.doIndexOpIX(this.getUint8(this.pc++));
					break;
				case 222:
					this.sbc_a(this.getUint8(this.pc++));
					break;
				case 223:
					this.push(this.pc);
					this.pc = 24;
					break;
				case 224:
					this.ret(0 === (this.f & F_PARITY));
					break;
				case 225:
					this.setHL(this.getUint16(this.sp));
					this.sp += 2;
					break;
				case 226:
					this.jp(0 ===
						(this.f & F_PARITY));
					break;
				case 227:
					a = this.getHL();
					this.setHL(this.getUint16(this.sp));
					this.setUint16(this.sp, a);
					break;
				case 228:
					this.call(0 === (this.f & F_PARITY));
					break;
				case 229:
					this.push(this.getHL());
					break;
				case 230:
					this.f = this.SZP_TABLE[this.a &= this.getUint8(this.pc++)] | F_HALFCARRY;
					break;
				case 231:
					this.push(this.pc);
					this.pc = 32;
					break;
				case 232:
					this.ret(0 !== (this.f & F_PARITY));
					break;
				case 233:
					this.pc = this.getHL();
					break;
				case 234:
					this.jp(0 !== (this.f & F_PARITY));
					break;
				case 235:
					a = this.d;
					this.d = this.h;
					this.h = a;
					a = this.e;
					this.e = this.l;
					this.l = a;
					break;
				case 236:
					this.call(0 !== (this.f & F_PARITY));
					break;
				case 237:
					this.doED(this.getUint8(this.pc));
					break;
				case 238:
					this.f = this.SZP_TABLE[this.a ^= this.getUint8(this.pc++)];
					break;
				case 239:
					this.push(this.pc);
					this.pc = 40;
					break;
				case 240:
					this.ret(0 === (this.f & F_SIGN));
					break;
				case 241:
					this.setAF(this.getUint16(this.sp));
					this.sp += 2;
					break;
				case 242:
					this.jp(0 === (this.f & F_SIGN));
					break;
				case 243:
					this.iff1 = this.iff2 = !1;
					this.EI_inst = !0;
					break;
				case 244:
					this.call(0 === (this.f & F_SIGN));
					break;
				case 245:
					this.push(this.getAF());
					break;
				case 246:
					this.f = this.SZP_TABLE[this.a |= this.getUint8(this.pc++)];
					break;
				case 247:
					this.push(this.pc);
					this.pc = 48;
					break;
				case 248:
					this.ret(0 !== (this.f & F_SIGN));
					break;
				case 249:
					this.sp = this.getHL();
					break;
				case 250:
					this.jp(0 !== (this.f & F_SIGN));
					break;
				case 251:
					this.iff1 = this.iff2 = this.EI_inst = !0;
					break;
				case 252:
					this.call(0 !== (this.f & F_SIGN));
					break;
				case 253:
					this.doIndexOpIY(this.getUint8(this.pc++));
					break;
				case 254:
					this.cp_a(this.getUint8(this.pc++));
					break;
				case 255:
					this.push(this.pc),
						this.pc = 56
			}
		},
		getCycle: function() {
			return this.totalCycles - this.tstates
		},
		nmi: function() {
			this.iff2 = this.iff1;
			this.iff1 = !1;
			REFRESH_EMULATION && this.incR();
			this.halt && (this.pc++, this.halt = !1);
			this.push(this.pc);
			this.pc = 102;
			this.tstates -= 11
		},
		interrupt: function() {
			!this.iff1 || (this.halt && (this.pc++, this.halt = !1), REFRESH_EMULATION && this.incR(), this.interruptLine = this.iff1 = this.iff2 = !1, this.push(this.pc), 0 === this.im ? (this.pc = 0 === this.interruptVector || 255 === this.interruptVector ?
				56 : this.interruptVector, this.tstates -= 13) : 1 === this.im ? (this.pc = 56, this.tstates -= 13) : (this.pc = this.getUint16((this.i << 8) + this.interruptVector), this.tstates -= 19))
		},
		jp: function(a) {
			this.pc = a ? this.getUint16(this.pc) : this.pc + 2
		},
		jr: function(a) {
			a ? (this.pc += this.getInt8(this.pc), this.tstates -= 5) : this.pc++
		},
		call: function(a) {
			a ? (this.push(this.pc + 2), this.pc = this.getUint16(this.pc), this.tstates -= 7) : this.pc += 2
		},
		ret: function(a) {
			a && (this.pc = this.getUint16(this.sp), this.sp += 2, this.tstates -= 6)
		},
		push: function(a) {
			this.sp -=
				2;
			this.setUint16(this.sp, a)
		},
		pushUint8: function(a, b) {
			this.sp -= 2;
			this.setUint16(this.sp, a << 8 | b)
		},
		incMem: function(a) {
			this.setUint8(a, this.inc8(this.getUint8(a)))
		},
		decMem: function(a) {
			this.setUint8(a, this.dec8(this.getUint8(a)))
		},
		ccf: function() {
			0 !== (this.f & F_CARRY) ? (this.f &= ~F_CARRY, this.f |= F_HALFCARRY) : (this.f |= F_CARRY, this.f &= ~F_HALFCARRY);
			this.f &= ~F_NEGATIVE
		},
		daa: function() {
			var a = this.DAA_TABLE[this.a | (this.f & F_CARRY) << 8 | (this.f & F_NEGATIVE) << 8 | (this.f & F_HALFCARRY) << 6];
			this.a = a & 255;
			this.f = this.f & F_NEGATIVE |
				a >> 8
		},
		doCB: function(a) {
			this.tstates -= OP_CB_STATES[a];
			REFRESH_EMULATION && this.incR();
			switch (a) {
				case 0:
					this.b = this.rlc(this.b);
					break;
				case 1:
					this.c = this.rlc(this.c);
					break;
				case 2:
					this.d = this.rlc(this.d);
					break;
				case 3:
					this.e = this.rlc(this.e);
					break;
				case 4:
					this.h = this.rlc(this.h);
					break;
				case 5:
					this.l = this.rlc(this.l);
					break;
				case 6:
					this.setUint8(this.getHL(), this.rlc(this.getUint8(this.getHL())));
					break;
				case 7:
					this.a = this.rlc(this.a);
					break;
				case 8:
					this.b = this.rrc(this.b);
					break;
				case 9:
					this.c = this.rrc(this.c);
					break;
				case 10:
					this.d = this.rrc(this.d);
					break;
				case 11:
					this.e = this.rrc(this.e);
					break;
				case 12:
					this.h = this.rrc(this.h);
					break;
				case 13:
					this.l = this.rrc(this.l);
					break;
				case 14:
					this.setUint8(this.getHL(), this.rrc(this.getUint8(this.getHL())));
					break;
				case 15:
					this.a = this.rrc(this.a);
					break;
				case 16:
					this.b = this.rl(this.b);
					break;
				case 17:
					this.c = this.rl(this.c);
					break;
				case 18:
					this.d = this.rl(this.d);
					break;
				case 19:
					this.e = this.rl(this.e);
					break;
				case 20:
					this.h = this.rl(this.h);
					break;
				case 21:
					this.l = this.rl(this.l);
					break;
				case 22:
					this.setUint8(this.getHL(),
						this.rl(this.getUint8(this.getHL())));
					break;
				case 23:
					this.a = this.rl(this.a);
					break;
				case 24:
					this.b = this.rr(this.b);
					break;
				case 25:
					this.c = this.rr(this.c);
					break;
				case 26:
					this.d = this.rr(this.d);
					break;
				case 27:
					this.e = this.rr(this.e);
					break;
				case 28:
					this.h = this.rr(this.h);
					break;
				case 29:
					this.l = this.rr(this.l);
					break;
				case 30:
					this.setUint8(this.getHL(), this.rr(this.getUint8(this.getHL())));
					break;
				case 31:
					this.a = this.rr(this.a);
					break;
				case 32:
					this.b = this.sla(this.b);
					break;
				case 33:
					this.c = this.sla(this.c);
					break;
				case 34:
					this.d =
						this.sla(this.d);
					break;
				case 35:
					this.e = this.sla(this.e);
					break;
				case 36:
					this.h = this.sla(this.h);
					break;
				case 37:
					this.l = this.sla(this.l);
					break;
				case 38:
					this.setUint8(this.getHL(), this.sla(this.getUint8(this.getHL())));
					break;
				case 39:
					this.a = this.sla(this.a);
					break;
				case 40:
					this.b = this.sra(this.b);
					break;
				case 41:
					this.c = this.sra(this.c);
					break;
				case 42:
					this.d = this.sra(this.d);
					break;
				case 43:
					this.e = this.sra(this.e);
					break;
				case 44:
					this.h = this.sra(this.h);
					break;
				case 45:
					this.l = this.sra(this.l);
					break;
				case 46:
					this.setUint8(this.getHL(),
						this.sra(this.getUint8(this.getHL())));
					break;
				case 47:
					this.a = this.sra(this.a);
					break;
				case 48:
					this.b = this.sll(this.b);
					break;
				case 49:
					this.c = this.sll(this.c);
					break;
				case 50:
					this.d = this.sll(this.d);
					break;
				case 51:
					this.e = this.sll(this.e);
					break;
				case 52:
					this.h = this.sll(this.h);
					break;
				case 53:
					this.l = this.sll(this.l);
					break;
				case 54:
					this.setUint8(this.getHL(), this.sll(this.getUint8(this.getHL())));
					break;
				case 55:
					this.a = this.sll(this.a);
					break;
				case 56:
					this.b = this.srl(this.b);
					break;
				case 57:
					this.c = this.srl(this.c);
					break;
				case 58:
					this.d = this.srl(this.d);
					break;
				case 59:
					this.e = this.srl(this.e);
					break;
				case 60:
					this.h = this.srl(this.h);
					break;
				case 61:
					this.l = this.srl(this.l);
					break;
				case 62:
					this.setUint8(this.getHL(), this.srl(this.getUint8(this.getHL())));
					break;
				case 63:
					this.a = this.srl(this.a);
					break;
				case 64:
					this.bit(this.b & BIT_0);
					break;
				case 65:
					this.bit(this.c & BIT_0);
					break;
				case 66:
					this.bit(this.d & BIT_0);
					break;
				case 67:
					this.bit(this.e & BIT_0);
					break;
				case 68:
					this.bit(this.h & BIT_0);
					break;
				case 69:
					this.bit(this.l & BIT_0);
					break;
				case 70:
					this.bit(this.getUint8(this.getHL()) &
						BIT_0);
					break;
				case 71:
					this.bit(this.a & BIT_0);
					break;
				case 72:
					this.bit(this.b & BIT_1);
					break;
				case 73:
					this.bit(this.c & BIT_1);
					break;
				case 74:
					this.bit(this.d & BIT_1);
					break;
				case 75:
					this.bit(this.e & BIT_1);
					break;
				case 76:
					this.bit(this.h & BIT_1);
					break;
				case 77:
					this.bit(this.l & BIT_1);
					break;
				case 78:
					this.bit(this.getUint8(this.getHL()) & BIT_1);
					break;
				case 79:
					this.bit(this.a & BIT_1);
					break;
				case 80:
					this.bit(this.b & BIT_2);
					break;
				case 81:
					this.bit(this.c & BIT_2);
					break;
				case 82:
					this.bit(this.d & BIT_2);
					break;
				case 83:
					this.bit(this.e &
						BIT_2);
					break;
				case 84:
					this.bit(this.h & BIT_2);
					break;
				case 85:
					this.bit(this.l & BIT_2);
					break;
				case 86:
					this.bit(this.getUint8(this.getHL()) & BIT_2);
					break;
				case 87:
					this.bit(this.a & BIT_2);
					break;
				case 88:
					this.bit(this.b & BIT_3);
					break;
				case 89:
					this.bit(this.c & BIT_3);
					break;
				case 90:
					this.bit(this.d & BIT_3);
					break;
				case 91:
					this.bit(this.e & BIT_3);
					break;
				case 92:
					this.bit(this.h & BIT_3);
					break;
				case 93:
					this.bit(this.l & BIT_3);
					break;
				case 94:
					this.bit(this.getUint8(this.getHL()) & BIT_3);
					break;
				case 95:
					this.bit(this.a & BIT_3);
					break;
				case 96:
					this.bit(this.b &
						BIT_4);
					break;
				case 97:
					this.bit(this.c & BIT_4);
					break;
				case 98:
					this.bit(this.d & BIT_4);
					break;
				case 99:
					this.bit(this.e & BIT_4);
					break;
				case 100:
					this.bit(this.h & BIT_4);
					break;
				case 101:
					this.bit(this.l & BIT_4);
					break;
				case 102:
					this.bit(this.getUint8(this.getHL()) & BIT_4);
					break;
				case 103:
					this.bit(this.a & BIT_4);
					break;
				case 104:
					this.bit(this.b & BIT_5);
					break;
				case 105:
					this.bit(this.c & BIT_5);
					break;
				case 106:
					this.bit(this.d & BIT_5);
					break;
				case 107:
					this.bit(this.e & BIT_5);
					break;
				case 108:
					this.bit(this.h & BIT_5);
					break;
				case 109:
					this.bit(this.l &
						BIT_5);
					break;
				case 110:
					this.bit(this.getUint8(this.getHL()) & BIT_5);
					break;
				case 111:
					this.bit(this.a & BIT_5);
					break;
				case 112:
					this.bit(this.b & BIT_6);
					break;
				case 113:
					this.bit(this.c & BIT_6);
					break;
				case 114:
					this.bit(this.d & BIT_6);
					break;
				case 115:
					this.bit(this.e & BIT_6);
					break;
				case 116:
					this.bit(this.h & BIT_6);
					break;
				case 117:
					this.bit(this.l & BIT_6);
					break;
				case 118:
					this.bit(this.getUint8(this.getHL()) & BIT_6);
					break;
				case 119:
					this.bit(this.a & BIT_6);
					break;
				case 120:
					this.bit(this.b & BIT_7);
					break;
				case 121:
					this.bit(this.c & BIT_7);
					break;
				case 122:
					this.bit(this.d & BIT_7);
					break;
				case 123:
					this.bit(this.e & BIT_7);
					break;
				case 124:
					this.bit(this.h & BIT_7);
					break;
				case 125:
					this.bit(this.l & BIT_7);
					break;
				case 126:
					this.bit(this.getUint8(this.getHL()) & BIT_7);
					break;
				case 127:
					this.bit(this.a & BIT_7);
					break;
				case 128:
					this.b &= ~BIT_0;
					break;
				case 129:
					this.c &= ~BIT_0;
					break;
				case 130:
					this.d &= ~BIT_0;
					break;
				case 131:
					this.e &= ~BIT_0;
					break;
				case 132:
					this.h &= ~BIT_0;
					break;
				case 133:
					this.l &= ~BIT_0;
					break;
				case 134:
					this.setUint8(this.getHL(), this.getUint8(this.getHL()) & ~BIT_0);
					break;
				case 135:
					this.a &= ~BIT_0;
					break;
				case 136:
					this.b &= ~BIT_1;
					break;
				case 137:
					this.c &= ~BIT_1;
					break;
				case 138:
					this.d &= ~BIT_1;
					break;
				case 139:
					this.e &= ~BIT_1;
					break;
				case 140:
					this.h &= ~BIT_1;
					break;
				case 141:
					this.l &= ~BIT_1;
					break;
				case 142:
					this.setUint8(this.getHL(), this.getUint8(this.getHL()) & ~BIT_1);
					break;
				case 143:
					this.a &= ~BIT_1;
					break;
				case 144:
					this.b &= ~BIT_2;
					break;
				case 145:
					this.c &= ~BIT_2;
					break;
				case 146:
					this.d &= ~BIT_2;
					break;
				case 147:
					this.e &= ~BIT_2;
					break;
				case 148:
					this.h &= ~BIT_2;
					break;
				case 149:
					this.l &= ~BIT_2;
					break;
				case 150:
					this.setUint8(this.getHL(), this.getUint8(this.getHL()) & ~BIT_2);
					break;
				case 151:
					this.a &= ~BIT_2;
					break;
				case 152:
					this.b &= ~BIT_3;
					break;
				case 153:
					this.c &= ~BIT_3;
					break;
				case 154:
					this.d &= ~BIT_3;
					break;
				case 155:
					this.e &= ~BIT_3;
					break;
				case 156:
					this.h &= ~BIT_3;
					break;
				case 157:
					this.l &= ~BIT_3;
					break;
				case 158:
					this.setUint8(this.getHL(), this.getUint8(this.getHL()) & ~BIT_3);
					break;
				case 159:
					this.a &= ~BIT_3;
					break;
				case 160:
					this.b &= ~BIT_4;
					break;
				case 161:
					this.c &= ~BIT_4;
					break;
				case 162:
					this.d &= ~BIT_4;
					break;
				case 163:
					this.e &=
						~BIT_4;
					break;
				case 164:
					this.h &= ~BIT_4;
					break;
				case 165:
					this.l &= ~BIT_4;
					break;
				case 166:
					this.setUint8(this.getHL(), this.getUint8(this.getHL()) & ~BIT_4);
					break;
				case 167:
					this.a &= ~BIT_4;
					break;
				case 168:
					this.b &= ~BIT_5;
					break;
				case 169:
					this.c &= ~BIT_5;
					break;
				case 170:
					this.d &= ~BIT_5;
					break;
				case 171:
					this.e &= ~BIT_5;
					break;
				case 172:
					this.h &= ~BIT_5;
					break;
				case 173:
					this.l &= ~BIT_5;
					break;
				case 174:
					this.setUint8(this.getHL(), this.getUint8(this.getHL()) & ~BIT_5);
					break;
				case 175:
					this.a &= ~BIT_5;
					break;
				case 176:
					this.b &= ~BIT_6;
					break;
				case 177:
					this.c &=
						~BIT_6;
					break;
				case 178:
					this.d &= ~BIT_6;
					break;
				case 179:
					this.e &= ~BIT_6;
					break;
				case 180:
					this.h &= ~BIT_6;
					break;
				case 181:
					this.l &= ~BIT_6;
					break;
				case 182:
					this.setUint8(this.getHL(), this.getUint8(this.getHL()) & ~BIT_6);
					break;
				case 183:
					this.a &= ~BIT_6;
					break;
				case 184:
					this.b &= ~BIT_7;
					break;
				case 185:
					this.c &= ~BIT_7;
					break;
				case 186:
					this.d &= ~BIT_7;
					break;
				case 187:
					this.e &= ~BIT_7;
					break;
				case 188:
					this.h &= ~BIT_7;
					break;
				case 189:
					this.l &= ~BIT_7;
					break;
				case 190:
					this.setUint8(this.getHL(), this.getUint8(this.getHL()) & ~BIT_7);
					break;
				case 191:
					this.a &=
						~BIT_7;
					break;
				case 192:
					this.b |= BIT_0;
					break;
				case 193:
					this.c |= BIT_0;
					break;
				case 194:
					this.d |= BIT_0;
					break;
				case 195:
					this.e |= BIT_0;
					break;
				case 196:
					this.h |= BIT_0;
					break;
				case 197:
					this.l |= BIT_0;
					break;
				case 198:
					this.setUint8(this.getHL(), this.getUint8(this.getHL()) | BIT_0);
					break;
				case 199:
					this.a |= BIT_0;
					break;
				case 200:
					this.b |= BIT_1;
					break;
				case 201:
					this.c |= BIT_1;
					break;
				case 202:
					this.d |= BIT_1;
					break;
				case 203:
					this.e |= BIT_1;
					break;
				case 204:
					this.h |= BIT_1;
					break;
				case 205:
					this.l |= BIT_1;
					break;
				case 206:
					this.setUint8(this.getHL(),
						this.getUint8(this.getHL()) | BIT_1);
					break;
				case 207:
					this.a |= BIT_1;
					break;
				case 208:
					this.b |= BIT_2;
					break;
				case 209:
					this.c |= BIT_2;
					break;
				case 210:
					this.d |= BIT_2;
					break;
				case 211:
					this.e |= BIT_2;
					break;
				case 212:
					this.h |= BIT_2;
					break;
				case 213:
					this.l |= BIT_2;
					break;
				case 214:
					this.setUint8(this.getHL(), this.getUint8(this.getHL()) | BIT_2);
					break;
				case 215:
					this.a |= BIT_2;
					break;
				case 216:
					this.b |= BIT_3;
					break;
				case 217:
					this.c |= BIT_3;
					break;
				case 218:
					this.d |= BIT_3;
					break;
				case 219:
					this.e |= BIT_3;
					break;
				case 220:
					this.h |= BIT_3;
					break;
				case 221:
					this.l |=
						BIT_3;
					break;
				case 222:
					this.setUint8(this.getHL(), this.getUint8(this.getHL()) | BIT_3);
					break;
				case 223:
					this.a |= BIT_3;
					break;
				case 224:
					this.b |= BIT_4;
					break;
				case 225:
					this.c |= BIT_4;
					break;
				case 226:
					this.d |= BIT_4;
					break;
				case 227:
					this.e |= BIT_4;
					break;
				case 228:
					this.h |= BIT_4;
					break;
				case 229:
					this.l |= BIT_4;
					break;
				case 230:
					this.setUint8(this.getHL(), this.getUint8(this.getHL()) | BIT_4);
					break;
				case 231:
					this.a |= BIT_4;
					break;
				case 232:
					this.b |= BIT_5;
					break;
				case 233:
					this.c |= BIT_5;
					break;
				case 234:
					this.d |= BIT_5;
					break;
				case 235:
					this.e |=
						BIT_5;
					break;
				case 236:
					this.h |= BIT_5;
					break;
				case 237:
					this.l |= BIT_5;
					break;
				case 238:
					this.setUint8(this.getHL(), this.getUint8(this.getHL()) | BIT_5);
					break;
				case 239:
					this.a |= BIT_5;
					break;
				case 240:
					this.b |= BIT_6;
					break;
				case 241:
					this.c |= BIT_6;
					break;
				case 242:
					this.d |= BIT_6;
					break;
				case 243:
					this.e |= BIT_6;
					break;
				case 244:
					this.h |= BIT_6;
					break;
				case 245:
					this.l |= BIT_6;
					break;
				case 246:
					this.setUint8(this.getHL(), this.getUint8(this.getHL()) | BIT_6);
					break;
				case 247:
					this.a |= BIT_6;
					break;
				case 248:
					this.b |= BIT_7;
					break;
				case 249:
					this.c |=
						BIT_7;
					break;
				case 250:
					this.d |= BIT_7;
					break;
				case 251:
					this.e |= BIT_7;
					break;
				case 252:
					this.h |= BIT_7;
					break;
				case 253:
					this.l |= BIT_7;
					break;
				case 254:
					this.setUint8(this.getHL(), this.getUint8(this.getHL()) | BIT_7);
					break;
				case 255:
					this.a |= BIT_7;
					break;
				default:
					JSSMS.Utils.console.log("Unimplemented CB Opcode: " + JSSMS.Utils.toHex(a))
			}
		},
		rlc: function(a) {
			var b = (a & 128) >> 7;
			a = (a << 1 | a >> 7) & 255;
			this.f = b | this.SZP_TABLE[a];
			return a
		},
		rrc: function(a) {
			var b = a & 1;
			a = (a >> 1 | a << 7) & 255;
			this.f = b | this.SZP_TABLE[a];
			return a
		},
		rl: function(a) {
			var b =
				(a & 128) >> 7;
			a = (a << 1 | this.f & F_CARRY) & 255;
			this.f = b | this.SZP_TABLE[a];
			return a
		},
		rr: function(a) {
			var b = a & 1;
			a = (a >> 1 | this.f << 7) & 255;
			this.f = b | this.SZP_TABLE[a];
			return a
		},
		sla: function(a) {
			var b = (a & 128) >> 7;
			a = a << 1 & 255;
			this.f = b | this.SZP_TABLE[a];
			return a
		},
		sll: function(a) {
			var b = (a & 128) >> 7;
			a = (a << 1 | 1) & 255;
			this.f = b | this.SZP_TABLE[a];
			return a
		},
		sra: function(a) {
			var b = a & 1;
			a = a >> 1 | a & 128;
			this.f = b | this.SZP_TABLE[a];
			return a
		},
		srl: function(a) {
			var b = a & 1;
			a = a >> 1 & 255;
			this.f = b | this.SZP_TABLE[a];
			return a
		},
		bit: function(a) {
			this.f = this.f &
				F_CARRY | this.SZ_BIT_TABLE[a]
		},
		doIndexOpIX: function(a) {
			this.tstates -= OP_DD_STATES[a];
			REFRESH_EMULATION && this.incR();
			switch (a) {
				case 9:
					this.setIXHIXL(this.add16(this.getIXHIXL(), this.getBC()));
					break;
				case 25:
					this.setIXHIXL(this.add16(this.getIXHIXL(), this.getDE()));
					break;
				case 33:
					this.setIXHIXL(this.getUint16(this.pc++));
					this.pc++;
					break;
				case 34:
					this.setUint16(this.getUint16(this.pc++), this.getIXHIXL());
					this.pc++;
					break;
				case 35:
					this.incIXHIXL();
					break;
				case 36:
					this.ixH = this.inc8(this.ixH);
					break;
				case 37:
					this.ixH =
						this.dec8(this.ixH);
					break;
				case 38:
					this.ixH = this.getUint8(this.pc++);
					break;
				case 41:
					this.setIXHIXL(this.add16(this.getIXHIXL(), this.getIXHIXL()));
					break;
				case 42:
					this.setIXHIXL(this.getUint16(this.getUint16(this.pc++)));
					this.pc++;
					break;
				case 43:
					this.decIXHIXL();
					break;
				case 44:
					this.ixL = this.inc8(this.ixL);
					break;
				case 45:
					this.ixL = this.dec8(this.ixL);
					break;
				case 46:
					this.ixL = this.getUint8(this.pc++);
					break;
				case 52:
					this.incMem(this.getIXHIXL() + this.d_());
					this.pc++;
					break;
				case 53:
					this.decMem(this.getIXHIXL() + this.d_());
					this.pc++;
					break;
				case 54:
					this.setUint8(this.getIXHIXL() + this.d_(), this.getUint8(++this.pc));
					this.pc++;
					break;
				case 57:
					this.setIXHIXL(this.add16(this.getIXHIXL(), this.sp));
					break;
				case 68:
					this.b = this.ixH;
					break;
				case 69:
					this.b = this.ixL;
					break;
				case 70:
					this.b = this.getUint8(this.getIXHIXL() + this.d_());
					this.pc++;
					break;
				case 76:
					this.c = this.ixH;
					break;
				case 77:
					this.c = this.ixL;
					break;
				case 78:
					this.c = this.getUint8(this.getIXHIXL() + this.d_());
					this.pc++;
					break;
				case 84:
					this.d = this.ixH;
					break;
				case 85:
					this.d = this.ixL;
					break;
				case 86:
					this.d =
						this.getUint8(this.getIXHIXL() + this.d_());
					this.pc++;
					break;
				case 92:
					this.e = this.ixH;
					break;
				case 93:
					this.e = this.ixL;
					break;
				case 94:
					this.e = this.getUint8(this.getIXHIXL() + this.d_());
					this.pc++;
					break;
				case 96:
					this.ixH = this.b;
					break;
				case 97:
					this.ixH = this.c;
					break;
				case 98:
					this.ixH = this.d;
					break;
				case 99:
					this.ixH = this.e;
					break;
				case 100:
					break;
				case 101:
					this.ixH = this.ixL;
					break;
				case 102:
					this.h = this.getUint8(this.getIXHIXL() + this.d_());
					this.pc++;
					break;
				case 103:
					this.ixH = this.a;
					break;
				case 104:
					this.ixL = this.b;
					break;
				case 105:
					this.ixL =
						this.c;
					break;
				case 106:
					this.ixL = this.d;
					break;
				case 107:
					this.ixL = this.e;
					break;
				case 108:
					this.ixL = this.ixH;
					break;
				case 109:
					break;
				case 110:
					this.l = this.getUint8(this.getIXHIXL() + this.d_());
					this.pc++;
					break;
				case 111:
					this.ixL = this.a;
					break;
				case 112:
					this.setUint8(this.getIXHIXL() + this.d_(), this.b);
					this.pc++;
					break;
				case 113:
					this.setUint8(this.getIXHIXL() + this.d_(), this.c);
					this.pc++;
					break;
				case 114:
					this.setUint8(this.getIXHIXL() + this.d_(), this.d);
					this.pc++;
					break;
				case 115:
					this.setUint8(this.getIXHIXL() + this.d_(), this.e);
					this.pc++;
					break;
				case 116:
					this.setUint8(this.getIXHIXL() + this.d_(), this.h);
					this.pc++;
					break;
				case 117:
					this.setUint8(this.getIXHIXL() + this.d_(), this.l);
					this.pc++;
					break;
				case 119:
					this.setUint8(this.getIXHIXL() + this.d_(), this.a);
					this.pc++;
					break;
				case 124:
					this.a = this.ixH;
					break;
				case 125:
					this.a = this.ixL;
					break;
				case 126:
					this.a = this.getUint8(this.getIXHIXL() + this.d_());
					this.pc++;
					break;
				case 132:
					this.add_a(this.ixH);
					break;
				case 133:
					this.add_a(this.ixL);
					break;
				case 134:
					this.add_a(this.getUint8(this.getIXHIXL() + this.d_()));
					this.pc++;
					break;
				case 140:
					this.adc_a(this.ixH);
					break;
				case 141:
					this.adc_a(this.ixL);
					break;
				case 142:
					this.adc_a(this.getUint8(this.getIXHIXL() + this.d_()));
					this.pc++;
					break;
				case 148:
					this.sub_a(this.ixH);
					break;
				case 149:
					this.sub_a(this.ixL);
					break;
				case 150:
					this.sub_a(this.getUint8(this.getIXHIXL() + this.d_()));
					this.pc++;
					break;
				case 156:
					this.sbc_a(this.ixH);
					break;
				case 157:
					this.sbc_a(this.ixL);
					break;
				case 158:
					this.sbc_a(this.getUint8(this.getIXHIXL() + this.d_()));
					this.pc++;
					break;
				case 164:
					this.f = this.SZP_TABLE[this.a &=
						this.ixH] | F_HALFCARRY;
					break;
				case 165:
					this.f = this.SZP_TABLE[this.a &= this.ixL] | F_HALFCARRY;
					break;
				case 166:
					this.f = this.SZP_TABLE[this.a &= this.getUint8(this.getIXHIXL() + this.d_())] | F_HALFCARRY;
					this.pc++;
					break;
				case 172:
					this.f = this.SZP_TABLE[this.a ^= this.ixH];
					break;
				case 173:
					this.f = this.SZP_TABLE[this.a ^= this.ixL];
					break;
				case 174:
					this.f = this.SZP_TABLE[this.a ^= this.getUint8(this.getIXHIXL() + this.d_())];
					this.pc++;
					break;
				case 180:
					this.f = this.SZP_TABLE[this.a |= this.ixH];
					break;
				case 181:
					this.f = this.SZP_TABLE[this.a |=
						this.ixL];
					break;
				case 182:
					this.f = this.SZP_TABLE[this.a |= this.getUint8(this.getIXHIXL() + this.d_())];
					this.pc++;
					break;
				case 188:
					this.cp_a(this.ixH);
					break;
				case 189:
					this.cp_a(this.ixL);
					break;
				case 190:
					this.cp_a(this.getUint8(this.getIXHIXL() + this.d_()));
					this.pc++;
					break;
				case 203:
					this.doIndexCB(this.getIXHIXL());
					break;
				case 225:
					this.setIXHIXL(this.getUint16(this.sp));
					this.sp += 2;
					break;
				case 227:
					a = this.getIXHIXL();
					this.setIXHIXL(this.getUint16(this.sp));
					this.setUint16(this.sp, a);
					break;
				case 229:
					this.push(this.getIXHIXL());
					break;
				case 233:
					this.pc = this.getIXHIXL();
					break;
				case 249:
					this.sp = this.getIXHIXL();
					break;
				default:
					JSSMS.Utils.console.log("Unimplemented DD/FD Opcode: " + JSSMS.Utils.toHex(a)), this.pc--
			}
		},
		doIndexOpIY: function(a) {
			this.tstates -= OP_DD_STATES[a];
			REFRESH_EMULATION && this.incR();
			switch (a) {
				case 9:
					this.setIYHIYL(this.add16(this.getIYHIYL(), this.getBC()));
					break;
				case 25:
					this.setIYHIYL(this.add16(this.getIYHIYL(), this.getDE()));
					break;
				case 33:
					this.setIYHIYL(this.getUint16(this.pc++));
					this.pc++;
					break;
				case 34:
					this.setUint16(this.getUint16(this.pc++),
						this.getIYHIYL());
					this.pc++;
					break;
				case 35:
					this.incIYHIYL();
					break;
				case 36:
					this.iyH = this.inc8(this.iyH);
					break;
				case 37:
					this.iyH = this.dec8(this.iyH);
					break;
				case 38:
					this.iyH = this.getUint8(this.pc++);
					break;
				case 41:
					this.setIYHIYL(this.add16(this.getIYHIYL(), this.getIYHIYL()));
					break;
				case 42:
					this.setIYHIYL(this.getUint16(this.getUint16(this.pc++)));
					this.pc++;
					break;
				case 43:
					this.decIYHIYL();
					break;
				case 44:
					this.iyL = this.inc8(this.iyL);
					break;
				case 45:
					this.iyL = this.dec8(this.iyL);
					break;
				case 46:
					this.iyL = this.getUint8(this.pc++);
					break;
				case 52:
					this.incMem(this.getIYHIYL() + this.d_());
					this.pc++;
					break;
				case 53:
					this.decMem(this.getIYHIYL() + this.d_());
					this.pc++;
					break;
				case 54:
					this.setUint8(this.getIYHIYL() + this.d_(), this.getUint8(++this.pc));
					this.pc++;
					break;
				case 57:
					this.setIYHIYL(this.add16(this.getIYHIYL(), this.sp));
					break;
				case 68:
					this.b = this.iyH;
					break;
				case 69:
					this.b = this.iyL;
					break;
				case 70:
					this.b = this.getUint8(this.getIYHIYL() + this.d_());
					this.pc++;
					break;
				case 76:
					this.c = this.iyH;
					break;
				case 77:
					this.c = this.iyL;
					break;
				case 78:
					this.c = this.getUint8(this.getIYHIYL() +
						this.d_());
					this.pc++;
					break;
				case 84:
					this.d = this.iyH;
					break;
				case 85:
					this.d = this.iyL;
					break;
				case 86:
					this.d = this.getUint8(this.getIYHIYL() + this.d_());
					this.pc++;
					break;
				case 92:
					this.e = this.iyH;
					break;
				case 93:
					this.e = this.iyL;
					break;
				case 94:
					this.e = this.getUint8(this.getIYHIYL() + this.d_());
					this.pc++;
					break;
				case 96:
					this.iyH = this.b;
					break;
				case 97:
					this.iyH = this.c;
					break;
				case 98:
					this.iyH = this.d;
					break;
				case 99:
					this.iyH = this.e;
					break;
				case 100:
					break;
				case 101:
					this.iyH = this.iyL;
					break;
				case 102:
					this.h = this.getUint8(this.getIYHIYL() +
						this.d_());
					this.pc++;
					break;
				case 103:
					this.iyH = this.a;
					break;
				case 104:
					this.iyL = this.b;
					break;
				case 105:
					this.iyL = this.c;
					break;
				case 106:
					this.iyL = this.d;
					break;
				case 107:
					this.iyL = this.e;
					break;
				case 108:
					this.iyL = this.iyH;
					break;
				case 109:
					break;
				case 110:
					this.l = this.getUint8(this.getIYHIYL() + this.d_());
					this.pc++;
					break;
				case 111:
					this.iyL = this.a;
					break;
				case 112:
					this.setUint8(this.getIYHIYL() + this.d_(), this.b);
					this.pc++;
					break;
				case 113:
					this.setUint8(this.getIYHIYL() + this.d_(), this.c);
					this.pc++;
					break;
				case 114:
					this.setUint8(this.getIYHIYL() +
						this.d_(), this.d);
					this.pc++;
					break;
				case 115:
					this.setUint8(this.getIYHIYL() + this.d_(), this.e);
					this.pc++;
					break;
				case 116:
					this.setUint8(this.getIYHIYL() + this.d_(), this.h);
					this.pc++;
					break;
				case 117:
					this.setUint8(this.getIYHIYL() + this.d_(), this.l);
					this.pc++;
					break;
				case 119:
					this.setUint8(this.getIYHIYL() + this.d_(), this.a);
					this.pc++;
					break;
				case 124:
					this.a = this.iyH;
					break;
				case 125:
					this.a = this.iyL;
					break;
				case 126:
					this.a = this.getUint8(this.getIYHIYL() + this.d_());
					this.pc++;
					break;
				case 132:
					this.add_a(this.iyH);
					break;
				case 133:
					this.add_a(this.iyL);
					break;
				case 134:
					this.add_a(this.getUint8(this.getIYHIYL() + this.d_()));
					this.pc++;
					break;
				case 140:
					this.adc_a(this.iyH);
					break;
				case 141:
					this.adc_a(this.iyL);
					break;
				case 142:
					this.adc_a(this.getUint8(this.getIYHIYL() + this.d_()));
					this.pc++;
					break;
				case 148:
					this.sub_a(this.iyH);
					break;
				case 149:
					this.sub_a(this.iyL);
					break;
				case 150:
					this.sub_a(this.getUint8(this.getIYHIYL() + this.d_()));
					this.pc++;
					break;
				case 156:
					this.sbc_a(this.iyH);
					break;
				case 157:
					this.sbc_a(this.iyL);
					break;
				case 158:
					this.sbc_a(this.getUint8(this.getIYHIYL() +
						this.d_()));
					this.pc++;
					break;
				case 164:
					this.f = this.SZP_TABLE[this.a &= this.iyH] | F_HALFCARRY;
					break;
				case 165:
					this.f = this.SZP_TABLE[this.a &= this.iyL] | F_HALFCARRY;
					break;
				case 166:
					this.f = this.SZP_TABLE[this.a &= this.getUint8(this.getIYHIYL() + this.d_())] | F_HALFCARRY;
					this.pc++;
					break;
				case 172:
					this.f = this.SZP_TABLE[this.a ^= this.iyH];
					break;
				case 173:
					this.f = this.SZP_TABLE[this.a ^= this.iyL];
					break;
				case 174:
					this.f = this.SZP_TABLE[this.a ^= this.getUint8(this.getIYHIYL() + this.d_())];
					this.pc++;
					break;
				case 180:
					this.f = this.SZP_TABLE[this.a |=
						this.iyH];
					break;
				case 181:
					this.f = this.SZP_TABLE[this.a |= this.iyL];
					break;
				case 182:
					this.f = this.SZP_TABLE[this.a |= this.getUint8(this.getIYHIYL() + this.d_())];
					this.pc++;
					break;
				case 188:
					this.cp_a(this.iyH);
					break;
				case 189:
					this.cp_a(this.iyL);
					break;
				case 190:
					this.cp_a(this.getUint8(this.getIYHIYL() + this.d_()));
					this.pc++;
					break;
				case 203:
					this.doIndexCB(this.getIYHIYL());
					break;
				case 225:
					this.setIYHIYL(this.getUint16(this.sp));
					this.sp += 2;
					break;
				case 227:
					a = this.getIYHIYL();
					this.setIYHIYL(this.getUint16(this.sp));
					this.setUint16(this.sp,
						a);
					break;
				case 229:
					this.push(this.getIYHIYL());
					break;
				case 233:
					this.pc = this.getIYHIYL();
					break;
				case 249:
					this.sp = this.getIYHIYL();
					break;
				default:
					JSSMS.Utils.console.log("Unimplemented DD/FD Opcode: " + JSSMS.Utils.toHex(a)), this.pc--
			}
		},
		doIndexCB: function(a) {
			a = a + this.getUint8(this.pc) & 65535;
			var b = this.getUint8(++this.pc);
			this.tstates -= OP_INDEX_CB_STATES[b];
			switch (b) {
				case 0:
					this.b = this.rlc(this.getUint8(a));
					this.setUint8(a, this.b);
					break;
				case 1:
					this.c = this.rlc(this.getUint8(a));
					this.setUint8(a, this.c);
					break;
				case 2:
					this.d = this.rlc(this.getUint8(a));
					this.setUint8(a, this.d);
					break;
				case 3:
					this.e = this.rlc(this.getUint8(a));
					this.setUint8(a, this.e);
					break;
				case 4:
					this.h = this.rlc(this.getUint8(a));
					this.setUint8(a, this.h);
					break;
				case 5:
					this.l = this.rlc(this.getUint8(a));
					this.setUint8(a, this.l);
					break;
				case 6:
					this.setUint8(a, this.rlc(this.getUint8(a)));
					break;
				case 7:
					this.a = this.rlc(this.getUint8(a));
					this.setUint8(a, this.a);
					break;
				case 8:
					this.b = this.rrc(this.getUint8(a));
					this.setUint8(a, this.b);
					break;
				case 9:
					this.c = this.rrc(this.getUint8(a));
					this.setUint8(a, this.c);
					break;
				case 10:
					this.d = this.rrc(this.getUint8(a));
					this.setUint8(a, this.d);
					break;
				case 11:
					this.e = this.rrc(this.getUint8(a));
					this.setUint8(a, this.e);
					break;
				case 12:
					this.h = this.rrc(this.getUint8(a));
					this.setUint8(a, this.h);
					break;
				case 13:
					this.l = this.rrc(this.getUint8(a));
					this.setUint8(a, this.l);
					break;
				case 14:
					this.setUint8(a, this.rrc(this.getUint8(a)));
					break;
				case 15:
					this.a = this.rrc(this.getUint8(a));
					this.setUint8(a, this.a);
					break;
				case 16:
					this.b = this.rl(this.getUint8(a));
					this.setUint8(a,
						this.b);
					break;
				case 17:
					this.c = this.rl(this.getUint8(a));
					this.setUint8(a, this.c);
					break;
				case 18:
					this.d = this.rl(this.getUint8(a));
					this.setUint8(a, this.d);
					break;
				case 19:
					this.e = this.rl(this.getUint8(a));
					this.setUint8(a, this.e);
					break;
				case 20:
					this.h = this.rl(this.getUint8(a));
					this.setUint8(a, this.h);
					break;
				case 21:
					this.l = this.rl(this.getUint8(a));
					this.setUint8(a, this.l);
					break;
				case 22:
					this.setUint8(a, this.rl(this.getUint8(a)));
					break;
				case 23:
					this.a = this.rl(this.getUint8(a));
					this.setUint8(a, this.a);
					break;
				case 24:
					this.b =
						this.rr(this.getUint8(a));
					this.setUint8(a, this.b);
					break;
				case 25:
					this.c = this.rr(this.getUint8(a));
					this.setUint8(a, this.c);
					break;
				case 26:
					this.d = this.rr(this.getUint8(a));
					this.setUint8(a, this.d);
					break;
				case 27:
					this.e = this.rr(this.getUint8(a));
					this.setUint8(a, this.e);
					break;
				case 28:
					this.h = this.rr(this.getUint8(a));
					this.setUint8(a, this.h);
					break;
				case 29:
					this.l = this.rr(this.getUint8(a));
					this.setUint8(a, this.l);
					break;
				case 30:
					this.setUint8(a, this.rr(this.getUint8(a)));
					break;
				case 31:
					this.a = this.rr(this.getUint8(a));
					this.setUint8(a, this.a);
					break;
				case 32:
					this.b = this.sla(this.getUint8(a));
					this.setUint8(a, this.b);
					break;
				case 33:
					this.c = this.sla(this.getUint8(a));
					this.setUint8(a, this.c);
					break;
				case 34:
					this.d = this.sla(this.getUint8(a));
					this.setUint8(a, this.d);
					break;
				case 35:
					this.e = this.sla(this.getUint8(a));
					this.setUint8(a, this.e);
					break;
				case 36:
					this.h = this.sla(this.getUint8(a));
					this.setUint8(a, this.h);
					break;
				case 37:
					this.l = this.sla(this.getUint8(a));
					this.setUint8(a, this.l);
					break;
				case 38:
					this.setUint8(a, this.sla(this.getUint8(a)));
					break;
				case 39:
					this.a = this.sla(this.getUint8(a));
					this.setUint8(a, this.a);
					break;
				case 40:
					this.b = this.sra(this.getUint8(a));
					this.setUint8(a, this.b);
					break;
				case 41:
					this.c = this.sra(this.getUint8(a));
					this.setUint8(a, this.c);
					break;
				case 42:
					this.d = this.sra(this.getUint8(a));
					this.setUint8(a, this.d);
					break;
				case 43:
					this.e = this.sra(this.getUint8(a));
					this.setUint8(a, this.e);
					break;
				case 44:
					this.h = this.sra(this.getUint8(a));
					this.setUint8(a, this.h);
					break;
				case 45:
					this.l = this.sra(this.getUint8(a));
					this.setUint8(a, this.l);
					break;
				case 46:
					this.setUint8(a, this.sra(this.getUint8(a)));
					break;
				case 47:
					this.a = this.sra(this.getUint8(a));
					this.setUint8(a, this.a);
					break;
				case 48:
					this.b = this.sll(this.getUint8(a));
					this.setUint8(a, this.b);
					break;
				case 49:
					this.c = this.sll(this.getUint8(a));
					this.setUint8(a, this.c);
					break;
				case 50:
					this.d = this.sll(this.getUint8(a));
					this.setUint8(a, this.d);
					break;
				case 51:
					this.e = this.sll(this.getUint8(a));
					this.setUint8(a, this.e);
					break;
				case 52:
					this.h = this.sll(this.getUint8(a));
					this.setUint8(a, this.h);
					break;
				case 53:
					this.l =
						this.sll(this.getUint8(a));
					this.setUint8(a, this.l);
					break;
				case 54:
					this.setUint8(a, this.sll(this.getUint8(a)));
					break;
				case 55:
					this.a = this.sll(this.getUint8(a));
					this.setUint8(a, this.a);
					break;
				case 56:
					this.b = this.srl(this.getUint8(a));
					this.setUint8(a, this.b);
					break;
				case 57:
					this.c = this.srl(this.getUint8(a));
					this.setUint8(a, this.c);
					break;
				case 58:
					this.d = this.srl(this.getUint8(a));
					this.setUint8(a, this.d);
					break;
				case 59:
					this.e = this.srl(this.getUint8(a));
					this.setUint8(a, this.e);
					break;
				case 60:
					this.h = this.srl(this.getUint8(a));
					this.setUint8(a, this.h);
					break;
				case 61:
					this.l = this.srl(this.getUint8(a));
					this.setUint8(a, this.l);
					break;
				case 62:
					this.setUint8(a, this.srl(this.getUint8(a)));
					break;
				case 63:
					this.a = this.srl(this.getUint8(a));
					this.setUint8(a, this.a);
					break;
				case 64:
				case 65:
				case 66:
				case 67:
				case 68:
				case 69:
				case 70:
				case 71:
					this.bit(this.getUint8(a) & BIT_0);
					break;
				case 72:
				case 73:
				case 74:
				case 75:
				case 76:
				case 77:
				case 78:
				case 79:
					this.bit(this.getUint8(a) & BIT_1);
					break;
				case 80:
				case 81:
				case 82:
				case 83:
				case 84:
				case 85:
				case 86:
				case 87:
					this.bit(this.getUint8(a) &
						BIT_2);
					break;
				case 88:
				case 89:
				case 90:
				case 91:
				case 92:
				case 93:
				case 94:
				case 95:
					this.bit(this.getUint8(a) & BIT_3);
					break;
				case 96:
				case 97:
				case 98:
				case 99:
				case 100:
				case 101:
				case 102:
				case 103:
					this.bit(this.getUint8(a) & BIT_4);
					break;
				case 104:
				case 105:
				case 106:
				case 107:
				case 108:
				case 109:
				case 110:
				case 111:
					this.bit(this.getUint8(a) & BIT_5);
					break;
				case 112:
				case 113:
				case 114:
				case 115:
				case 116:
				case 117:
				case 118:
				case 119:
					this.bit(this.getUint8(a) & BIT_6);
					break;
				case 120:
				case 121:
				case 122:
				case 123:
				case 124:
				case 125:
				case 126:
				case 127:
					this.bit(this.getUint8(a) &
						BIT_7);
					break;
				case 128:
				case 129:
				case 130:
				case 131:
				case 132:
				case 133:
				case 134:
				case 135:
					this.setUint8(a, this.getUint8(a) & ~BIT_0);
					break;
				case 136:
				case 137:
				case 138:
				case 139:
				case 140:
				case 141:
				case 142:
				case 143:
					this.setUint8(a, this.getUint8(a) & ~BIT_1);
					break;
				case 144:
				case 145:
				case 146:
				case 147:
				case 148:
				case 149:
				case 150:
				case 151:
					this.setUint8(a, this.getUint8(a) & ~BIT_2);
					break;
				case 152:
				case 153:
				case 154:
				case 155:
				case 156:
				case 157:
				case 158:
				case 159:
					this.setUint8(a, this.getUint8(a) & ~BIT_3);
					break;
				case 160:
				case 161:
				case 162:
				case 163:
				case 164:
				case 165:
				case 166:
				case 167:
					this.setUint8(a,
						this.getUint8(a) & ~BIT_4);
					break;
				case 168:
				case 169:
				case 170:
				case 171:
				case 172:
				case 173:
				case 174:
				case 175:
					this.setUint8(a, this.getUint8(a) & ~BIT_5);
					break;
				case 176:
				case 177:
				case 178:
				case 179:
				case 180:
				case 181:
				case 182:
				case 183:
					this.setUint8(a, this.getUint8(a) & ~BIT_6);
					break;
				case 184:
				case 185:
				case 186:
				case 187:
				case 188:
				case 189:
				case 190:
				case 191:
					this.setUint8(a, this.getUint8(a) & ~BIT_7);
					break;
				case 192:
				case 193:
				case 194:
				case 195:
				case 196:
				case 197:
				case 198:
				case 199:
					this.setUint8(a, this.getUint8(a) | BIT_0);
					break;
				case 200:
				case 201:
				case 202:
				case 203:
				case 204:
				case 205:
				case 206:
				case 207:
					this.setUint8(a, this.getUint8(a) | BIT_1);
					break;
				case 208:
				case 209:
				case 210:
				case 211:
				case 212:
				case 213:
				case 214:
				case 215:
					this.setUint8(a, this.getUint8(a) | BIT_2);
					break;
				case 216:
				case 217:
				case 218:
				case 219:
				case 220:
				case 221:
				case 222:
				case 223:
					this.setUint8(a, this.getUint8(a) | BIT_3);
					break;
				case 224:
				case 225:
				case 226:
				case 227:
				case 228:
				case 229:
				case 230:
				case 231:
					this.setUint8(a, this.getUint8(a) | BIT_4);
					break;
				case 232:
				case 233:
				case 234:
				case 235:
				case 236:
				case 237:
				case 238:
				case 239:
					this.setUint8(a,
						this.getUint8(a) | BIT_5);
					break;
				case 240:
				case 241:
				case 242:
				case 243:
				case 244:
				case 245:
				case 246:
				case 247:
					this.setUint8(a, this.getUint8(a) | BIT_6);
					break;
				case 248:
				case 249:
				case 250:
				case 251:
				case 252:
				case 253:
				case 254:
				case 255:
					this.setUint8(a, this.getUint8(a) | BIT_7);
					break;
				default:
					JSSMS.Utils.console.log("Unimplemented DDCB/FDCB Opcode: " + JSSMS.Utils.toHex(b))
			}
			this.pc++
		},
		doED: function(a) {
			var b;
			this.tstates -= OP_ED_STATES[a];
			REFRESH_EMULATION && this.incR();
			switch (a) {
				case 64:
					this.b = this.port.in_(this.c);
					this.f =
						this.f & F_CARRY | this.SZP_TABLE[this.b];
					this.pc++;
					break;
				case 65:
					this.port.out(this.c, this.b);
					this.pc++;
					break;
				case 66:
					this.sbc16(this.getBC());
					this.pc++;
					break;
				case 67:
					this.setUint16(this.getUint16(++this.pc), this.getBC());
					this.pc += 2;
					break;
				case 68:
				case 76:
				case 84:
				case 92:
				case 100:
				case 108:
				case 116:
				case 124:
					a = this.a;
					this.a = 0;
					this.sub_a(a);
					this.pc++;
					break;
				case 69:
				case 77:
				case 85:
				case 93:
				case 101:
				case 109:
				case 117:
				case 125:
					this.pc = this.getUint16(this.sp);
					this.sp += 2;
					this.iff1 = this.iff2;
					break;
				case 70:
				case 78:
				case 102:
				case 110:
					this.im =
						0;
					this.pc++;
					break;
				case 71:
					this.i = this.a;
					this.pc++;
					break;
				case 72:
					this.c = this.port.in_(this.c);
					this.f = this.f & F_CARRY | this.SZP_TABLE[this.c];
					this.pc++;
					break;
				case 73:
					this.port.out(this.c, this.c);
					this.pc++;
					break;
				case 74:
					this.adc16(this.getBC());
					this.pc++;
					break;
				case 75:
					this.setBC(this.getUint16(this.getUint16(++this.pc)));
					this.pc += 2;
					break;
				case 79:
					this.r = this.a;
					this.pc++;
					break;
				case 80:
					this.d = this.port.in_(this.c);
					this.f = this.f & F_CARRY | this.SZP_TABLE[this.d];
					this.pc++;
					break;
				case 81:
					this.port.out(this.c, this.d);
					this.pc++;
					break;
				case 82:
					this.sbc16(this.getDE());
					this.pc++;
					break;
				case 83:
					this.setUint16(this.getUint16(++this.pc), this.getDE());
					this.pc += 2;
					break;
				case 86:
				case 118:
					this.im = 1;
					this.pc++;
					break;
				case 87:
					this.a = this.i;
					this.f = this.f & F_CARRY | this.SZ_TABLE[this.a] | (this.iff2 ? F_PARITY : 0);
					this.pc++;
					break;
				case 88:
					this.e = this.port.in_(this.c);
					this.f = this.f & F_CARRY | this.SZP_TABLE[this.e];
					this.pc++;
					break;
				case 89:
					this.port.out(this.c, this.e);
					this.pc++;
					break;
				case 90:
					this.adc16(this.getDE());
					this.pc++;
					break;
				case 91:
					this.setDE(this.getUint16(this.getUint16(++this.pc)));
					this.pc += 2;
					break;
				case 95:
					this.a = REFRESH_EMULATION ? this.r : JSSMS.Utils.rndInt(255);
					this.f = this.f & F_CARRY | this.SZ_TABLE[this.a] | (this.iff2 ? F_PARITY : 0);
					this.pc++;
					break;
				case 96:
					this.h = this.port.in_(this.c);
					this.f = this.f & F_CARRY | this.SZP_TABLE[this.h];
					this.pc++;
					break;
				case 97:
					this.port.out(this.c, this.h);
					this.pc++;
					break;
				case 98:
					this.sbc16(this.getHL());
					this.pc++;
					break;
				case 99:
					this.setUint16(this.getUint16(++this.pc), this.getHL());
					this.pc += 2;
					break;
				case 103:
					b = this.getHL();
					a = this.getUint8(b);
					this.setUint8(b,
						a >> 4 | (this.a & 15) << 4);
					this.a = this.a & 240 | a & 15;
					this.f = this.f & F_CARRY | this.SZP_TABLE[this.a];
					this.pc++;
					break;
				case 104:
					this.l = this.port.in_(this.c);
					this.f = this.f & F_CARRY | this.SZP_TABLE[this.l];
					this.pc++;
					break;
				case 105:
					this.port.out(this.c, this.l);
					this.pc++;
					break;
				case 106:
					this.adc16(this.getHL());
					this.pc++;
					break;
				case 107:
					this.setHL(this.getUint16(this.getUint16(++this.pc)));
					this.pc += 2;
					break;
				case 111:
					b = this.getHL();
					a = this.getUint8(b);
					this.setUint8(b, (a & 15) << 4 | this.a & 15);
					this.a = this.a & 240 | a >> 4;
					this.f = this.f &
						F_CARRY | this.SZP_TABLE[this.a];
					this.pc++;
					break;
				case 113:
					this.port.out(this.c, 0);
					this.pc++;
					break;
				case 114:
					this.sbc16(this.sp);
					this.pc++;
					break;
				case 115:
					this.setUint16(this.getUint16(++this.pc), this.sp);
					this.pc += 2;
					break;
				case 120:
					this.a = this.port.in_(this.c);
					this.f = this.f & F_CARRY | this.SZP_TABLE[this.a];
					this.pc++;
					break;
				case 121:
					this.port.out(this.c, this.a);
					this.pc++;
					break;
				case 122:
					this.adc16(this.sp);
					this.pc++;
					break;
				case 123:
					this.sp = this.getUint16(this.getUint16(++this.pc));
					this.pc += 2;
					break;
				case 160:
					a =
						this.getUint8(this.getHL());
					this.setUint8(this.getDE(), a);
					this.decBC();
					this.incDE();
					this.incHL();
					a = a + this.a & 255;
					this.f = this.f & 193 | (this.getBC() ? F_PARITY : 0) | a & 8 | (a & 2 ? 32 : 0);
					this.pc++;
					break;
				case 161:
					a = this.f & F_CARRY | F_NEGATIVE;
					this.cp_a(this.getUint8(this.getHL()));
					this.decBC();
					this.incHL();
					a |= 0 === this.getBC() ? 0 : F_PARITY;
					this.f = this.f & 248 | a;
					this.pc++;
					break;
				case 162:
					a = this.port.in_(this.c);
					this.setUint8(this.getHL(), a);
					this.b = this.dec8(this.b);
					this.incHL();
					this.f = 128 === (a & 128) ? this.f | F_NEGATIVE : this.f &
						~F_NEGATIVE;
					this.pc++;
					break;
				case 163:
					a = this.getUint8(this.getHL());
					this.port.out(this.c, a);
					this.b = this.dec8(this.b);
					this.incHL();
					255 < this.l + a ? (this.f |= F_CARRY, this.f |= F_HALFCARRY) : (this.f &= ~F_CARRY, this.f &= ~F_HALFCARRY);
					this.f = 128 === (a & 128) ? this.f | F_NEGATIVE : this.f & ~F_NEGATIVE;
					this.pc++;
					break;
				case 168:
					a = this.getUint8(this.getHL());
					this.setUint8(this.getDE(), a);
					this.decBC();
					this.decDE();
					this.decHL();
					a = a + this.a & 255;
					this.f = this.f & 193 | (this.getBC() ? F_PARITY : 0) | a & F_BIT3 | (a & F_NEGATIVE ? 32 : 0);
					this.pc++;
					break;
				case 169:
					a = this.f & F_CARRY | F_NEGATIVE;
					this.cp_a(this.getUint8(this.getHL()));
					this.decBC();
					this.decHL();
					a |= 0 === this.getBC() ? 0 : F_PARITY;
					this.f = this.f & 248 | a;
					this.pc++;
					break;
				case 170:
					a = this.port.in_(this.c);
					this.setUint8(this.getHL(), a);
					this.b = this.dec8(this.b);
					this.decHL();
					this.f = 0 !== (a & 128) ? this.f | F_NEGATIVE : this.f & ~F_NEGATIVE;
					this.pc++;
					break;
				case 171:
					a = this.getUint8(this.getHL());
					this.port.out(this.c, a);
					this.b = this.dec8(this.b);
					this.decHL();
					255 < this.l + a ? (this.f |= F_CARRY, this.f |= F_HALFCARRY) : (this.f &=
						~F_CARRY, this.f &= ~F_HALFCARRY);
					this.f = 128 === (a & 128) ? this.f | F_NEGATIVE : this.f & ~F_NEGATIVE;
					this.pc++;
					break;
				case 176:
					a = this.getUint8(this.getHL());
					this.setUint8(this.getDE(), a);
					this.decBC();
					this.incDE();
					this.incHL();
					a = a + this.a & 255;
					this.f = this.f & 193 | (this.getBC() ? F_PARITY : 0) | a & 8 | (a & 2 ? 32 : 0);
					0 !== this.getBC() ? (this.tstates -= 5, this.pc--) : this.pc++;
					break;
				case 177:
					a = this.f & F_CARRY | F_NEGATIVE;
					this.cp_a(this.getUint8(this.getHL()));
					this.decBC();
					this.incHL();
					a |= 0 === this.getBC() ? 0 : F_PARITY;
					0 !== (a & F_PARITY) && 0 ===
						(this.f & F_ZERO) ? (this.tstates -= 5, this.pc--) : this.pc++;
					this.f = this.f & 248 | a;
					break;
				case 178:
					a = this.port.in_(this.c);
					this.setUint8(this.getHL(), a);
					this.b = this.dec8(this.b);
					this.incHL();
					0 !== this.b ? (this.tstates -= 5, this.pc--) : this.pc++;
					this.f = 128 === (a & 128) ? this.f | F_NEGATIVE : this.f & ~F_NEGATIVE;
					break;
				case 179:
					a = this.getUint8(this.getHL());
					this.port.out(this.c, a);
					this.b = this.dec8(this.b);
					this.incHL();
					0 !== this.b ? (this.tstates -= 5, this.pc--) : this.pc++;
					255 < this.l + a ? (this.f |= F_CARRY, this.f |= F_HALFCARRY) : (this.f &=
						~F_CARRY, this.f &= ~F_HALFCARRY);
					this.f = 0 !== (a & 128) ? this.f | F_NEGATIVE : this.f & ~F_NEGATIVE;
					break;
				case 184:
					a = this.getUint8(this.getHL());
					this.setUint8(this.getDE(), a);
					this.decBC();
					this.decDE();
					this.decHL();
					a = a + this.a & 255;
					this.f = this.f & 193 | (this.getBC() ? F_PARITY : 0) | a & F_BIT3 | (a & F_NEGATIVE ? 32 : 0);
					0 !== this.getBC() ? (this.tstates -= 5, this.pc--) : this.pc++;
					break;
				case 185:
					a = this.f & F_CARRY | F_NEGATIVE;
					this.cp_a(this.getUint8(this.getHL()));
					this.decBC();
					this.decHL();
					a |= 0 === this.getBC() ? 0 : F_PARITY;
					0 !== (a & F_PARITY) &&
						0 === (this.f & F_ZERO) ? (this.tstates -= 5, this.pc--) : this.pc++;
					this.f = this.f & 248 | a;
					break;
				case 186:
					a = this.port.in_(this.c);
					this.setUint8(this.getHL(), a);
					this.b = this.dec8(this.b);
					this.decHL();
					0 !== this.b ? (this.tstates -= 5, this.pc--) : this.pc++;
					this.f = 0 !== (a & 128) ? this.f | F_NEGATIVE : this.f & ~F_NEGATIVE;
					break;
				case 187:
					a = this.getUint8(this.getHL());
					this.port.out(this.c, a);
					this.b = this.dec8(this.b);
					this.decHL();
					0 !== this.b ? (this.tstates -= 5, this.pc--) : this.pc++;
					255 < this.l + a ? (this.f |= F_CARRY, this.f |= F_HALFCARRY) : (this.f &=
						~F_CARRY, this.f &= ~F_HALFCARRY);
					this.f = 0 !== (a & 128) ? this.f | F_NEGATIVE : this.f & ~F_NEGATIVE;
					break;
				default:
					JSSMS.Utils.console.log("Unimplemented ED Opcode: " + JSSMS.Utils.toHex(a)), this.pc++
			}
		},
		generateDAATable: function() {
			var a, b, d, f;
			for (a = 256; a--;)
				for (b = 0; 1 >= b; b++)
					for (d = 0; 1 >= d; d++)
						for (f = 0; 1 >= f; f++) this.DAA_TABLE[b << 8 | f << 9 | d << 10 | a] = this.getDAAResult(a, b | f << 1 | d << 4);
			this.a = this.f = 0
		},
		getDAAResult: function(a, b) {
			this.a = a;
			this.f = b;
			a = this.a;
			var d = 0,
				f = b & F_CARRY,
				c = f;
			if (0 !== (b & F_HALFCARRY) || 9 < (a & 15)) d |= 6;
			if (1 === f ||
				159 < a || 143 < a && 9 < (a & 15)) d |= 96, c = 1;
			153 < a && (c = 1);
			0 !== (b & F_NEGATIVE) ? this.sub_a(d) : this.add_a(d);
			b = this.f & 254 | c;
			b = this.getParity(this.a) ? b & 251 | F_PARITY : b & 251;
			return this.a | b << 8
		},
		add_a: function(a) {
			a = this.a + a & 255;
			this.f = this.SZHVC_ADD_TABLE[this.a << 8 | a];
			this.a = a
		},
		adc_a: function(a) {
			var b = this.f & F_CARRY;
			a = this.a + a + b & 255;
			this.f = this.SZHVC_ADD_TABLE[b << 16 | this.a << 8 | a];
			this.a = a
		},
		sub_a: function(a) {
			a = this.a - a & 255;
			this.f = this.SZHVC_SUB_TABLE[this.a << 8 | a];
			this.a = a
		},
		sbc_a: function(a) {
			var b = this.f & F_CARRY;
			a = this.a -
				a - b & 255;
			this.f = this.SZHVC_SUB_TABLE[b << 16 | this.a << 8 | a];
			this.a = a
		},
		cp_a: function(a) {
			this.f = this.SZHVC_SUB_TABLE[this.a << 8 | this.a - a & 255]
		},
		cpl_a: function() {
			this.a ^= 255;
			this.f = this.f | F_NEGATIVE | F_HALFCARRY
		},
		rra_a: function() {
			var a = this.a & 1;
			this.a = (this.a >> 1 | (this.f & F_CARRY) << 7) & 255;
			this.f = this.f & 236 | a
		},
		rla_a: function() {
			var a = this.a >> 7;
			this.a = (this.a << 1 | this.f & F_CARRY) & 255;
			this.f = this.f & 236 | a
		},
		rlca_a: function() {
			var a = this.a >> 7;
			this.a = this.a << 1 & 255 | a;
			this.f = this.f & 236 | a
		},
		rrca_a: function() {
			var a = this.a & 1;
			this.a = this.a >> 1 | a << 7;
			this.f = this.f & 236 | a
		},
		getBC: function() {
			return this.b << 8 | this.c
		},
		getDE: function() {
			return this.d << 8 | this.e
		},
		getHL: function() {
			return this.h << 8 | this.l
		},
		getAF: function() {
			return this.a << 8 | this.f
		},
		getIXHIXL: function() {
			return this.ixH << 8 | this.ixL
		},
		getIYHIYL: function() {
			return this.iyH << 8 | this.iyL
		},
		setBC: function(a) {
			this.b = a >> 8;
			this.c = a & 255
		},
		setDE: function(a) {
			this.d = a >> 8;
			this.e = a & 255
		},
		setHL: function(a) {
			this.h = a >> 8;
			this.l = a & 255
		},
		setAF: function(a) {
			this.a = a >> 8;
			this.f = a & 255
		},
		setIXHIXL: function(a) {
			this.ixH =
				a >> 8;
			this.ixL = a & 255
		},
		setIYHIYL: function(a) {
			this.iyH = a >> 8;
			this.iyL = a & 255
		},
		incBC: function() {
			this.c = this.c + 1 & 255;
			0 === this.c && (this.b = this.b + 1 & 255)
		},
		incDE: function() {
			this.e = this.e + 1 & 255;
			0 === this.e && (this.d = this.d + 1 & 255)
		},
		incHL: function() {
			this.l = this.l + 1 & 255;
			0 === this.l && (this.h = this.h + 1 & 255)
		},
		incIXHIXL: function() {
			this.ixL = this.ixL + 1 & 255;
			0 === this.ixL && (this.ixH = this.ixH + 1 & 255)
		},
		incIYHIYL: function() {
			this.iyL = this.iyL + 1 & 255;
			0 === this.iyL && (this.iyH = this.iyH + 1 & 255)
		},
		decBC: function() {
			this.c = this.c - 1 & 255;
			255 ===
				this.c && (this.b = this.b - 1 & 255)
		},
		decDE: function() {
			this.e = this.e - 1 & 255;
			255 === this.e && (this.d = this.d - 1 & 255)
		},
		decHL: function() {
			this.l = this.l - 1 & 255;
			255 === this.l && (this.h = this.h - 1 & 255)
		},
		decIXHIXL: function() {
			this.ixL = this.ixL - 1 & 255;
			255 === this.ixL && (this.ixH = this.ixH - 1 & 255)
		},
		decIYHIYL: function() {
			this.iyL = this.iyL - 1 & 255;
			255 === this.iyL && (this.iyH = this.iyH - 1 & 255)
		},
		inc8: function(a) {
			a = a + 1 & 255;
			this.f = this.f & F_CARRY | this.SZHV_INC_TABLE[a];
			return a
		},
		dec8: function(a) {
			a = a - 1 & 255;
			this.f = this.f & F_CARRY | this.SZHV_DEC_TABLE[a];
			return a
		},
		exAF: function() {
			var a = this.a;
			this.a = this.a2;
			this.a2 = a;
			a = this.f;
			this.f = this.f2;
			this.f2 = a
		},
		exBC: function() {
			var a = this.b;
			this.b = this.b2;
			this.b2 = a;
			a = this.c;
			this.c = this.c2;
			this.c2 = a
		},
		exDE: function() {
			var a = this.d;
			this.d = this.d2;
			this.d2 = a;
			a = this.e;
			this.e = this.e2;
			this.e2 = a
		},
		exHL: function() {
			var a = this.h;
			this.h = this.h2;
			this.h2 = a;
			a = this.l;
			this.l = this.l2;
			this.l2 = a
		},
		add16: function(a, b) {
			var d = a + b;
			this.f = this.f & 196 | (a ^ d ^ b) >> 8 & 16 | d >> 16 & 1;
			return d & 65535
		},
		adc16: function(a) {
			var b = this.getHL(),
				d = b + a + (this.f &
					F_CARRY);
			this.f = (b ^ d ^ a) >> 8 & 16 | d >> 16 & 1 | d >> 8 & 128 | (0 !== (d & 65535) ? 0 : 64) | ((a ^ b ^ 32768) & (a ^ d) & 32768) >> 13;
			this.h = d >> 8 & 255;
			this.l = d & 255
		},
		sbc16: function(a) {
			var b = this.getHL(),
				d = b - a - (this.f & F_CARRY);
			this.f = (b ^ d ^ a) >> 8 & 16 | 2 | d >> 16 & 1 | d >> 8 & 128 | (0 !== (d & 65535) ? 0 : 64) | ((a ^ b) & (b ^ d) & 32768) >> 13;
			this.h = d >> 8 & 255;
			this.l = d & 255
		},
		incR: function() {
			this.r = this.r & 128 | this.r + 1 & 127
		},
		nop: function() {},
		generateFlagTables: function() {
			var a, b, d, f, c, g, h;
			for (a = 0; 256 > a; a++) b = 0 !== (a & 128) ? F_SIGN : 0, d = 0 === a ? F_ZERO : 0, f = a & 32, c = a & 8, g = this.getParity(a) ?
				F_PARITY : 0, this.SZ_TABLE[a] = b | d | f | c, this.SZP_TABLE[a] = b | d | f | c | g, this.SZHV_INC_TABLE[a] = b | d | f | c, this.SZHV_INC_TABLE[a] |= 128 === a ? F_OVERFLOW : 0, this.SZHV_INC_TABLE[a] |= 0 === (a & 15) ? F_HALFCARRY : 0, this.SZHV_DEC_TABLE[a] = b | d | f | c | F_NEGATIVE, this.SZHV_DEC_TABLE[a] |= 127 === a ? F_OVERFLOW : 0, this.SZHV_DEC_TABLE[a] |= 15 === (a & 15) ? F_HALFCARRY : 0, this.SZ_BIT_TABLE[a] = 0 !== a ? a & 128 : F_ZERO | F_PARITY, this.SZ_BIT_TABLE[a] = this.SZ_BIT_TABLE[a] | f | c | F_HALFCARRY;
			a = 0;
			b = 65536;
			d = 0;
			f = 65536;
			for (g = 0; 256 > g; g++)
				for (h = 0; 256 > h; h++) c = h - g, this.SZHVC_ADD_TABLE[a] =
					0 !== h ? 0 !== (h & 128) ? F_SIGN : 0 : F_ZERO, this.SZHVC_ADD_TABLE[a] |= h & (F_BIT5 | F_BIT3), (h & 15) < (g & 15) && (this.SZHVC_ADD_TABLE[a] |= F_HALFCARRY), h < g && (this.SZHVC_ADD_TABLE[a] |= F_CARRY), 0 !== ((c ^ g ^ 128) & (c ^ h) & 128) && (this.SZHVC_ADD_TABLE[a] |= F_OVERFLOW), a++, c = h - g - 1, this.SZHVC_ADD_TABLE[b] = 0 !== h ? 0 !== (h & 128) ? F_SIGN : 0 : F_ZERO, this.SZHVC_ADD_TABLE[b] |= h & (F_BIT5 | F_BIT3), (h & 15) <= (g & 15) && (this.SZHVC_ADD_TABLE[b] |= F_HALFCARRY), h <= g && (this.SZHVC_ADD_TABLE[b] |= F_CARRY), 0 !== ((c ^ g ^ 128) & (c ^ h) & 128) && (this.SZHVC_ADD_TABLE[b] |= F_OVERFLOW),
					b++, c = g - h, this.SZHVC_SUB_TABLE[d] = 0 !== h ? 0 !== (h & 128) ? F_NEGATIVE | F_SIGN : F_NEGATIVE : F_NEGATIVE | F_ZERO, this.SZHVC_SUB_TABLE[d] |= h & (F_BIT5 | F_BIT3), (h & 15) > (g & 15) && (this.SZHVC_SUB_TABLE[d] |= F_HALFCARRY), h > g && (this.SZHVC_SUB_TABLE[d] |= F_CARRY), 0 !== ((c ^ g) & (g ^ h) & 128) && (this.SZHVC_SUB_TABLE[d] |= F_OVERFLOW), d++, c = g - h - 1, this.SZHVC_SUB_TABLE[f] = 0 !== h ? 0 !== (h & 128) ? F_NEGATIVE | F_SIGN : F_NEGATIVE : F_NEGATIVE | F_ZERO, this.SZHVC_SUB_TABLE[f] |= h & (F_BIT5 | F_BIT3), (h & 15) >= (g & 15) && (this.SZHVC_SUB_TABLE[f] |= F_HALFCARRY), h >= g &&
					(this.SZHVC_SUB_TABLE[f] |= F_CARRY), 0 !== ((c ^ g) & (g ^ h) & 128) && (this.SZHVC_SUB_TABLE[f] |= F_OVERFLOW), f++
		},
		getParity: function(a) {
			var b = !0,
				d;
			for (d = 0; 8 > d; d++) 0 !== (a & 1 << d) && (b = !b);
			return b
		},
		generateMemory: function() {
      for(var a = 0; 8192 > a; a++) this.memWriteMap.setUint8(a, 0);
      for(a = 0; 32768 > a; a++) this.sram.setUint8(a, 0);
			this.useSRAM = !1;
			this.number_of_pages = 2;
			for (a = 0; 4 > a; a++) this.frameReg[a] = a % 3
		},
		resetMemory: function(a) {
			a && (this.rom = a);
			if (this.rom.length) {
				this.number_of_pages = this.rom.length;
				this.romPageMask = this.number_of_pages - 1;
				for (a = 0; 3 > a; a++) this.frameReg[a] = a % this.number_of_pages;
				this.frameReg[3] = 0;
			} else this.romPageMask = this.number_of_pages = 0
		},
		d_: function() {
			return this.getUint8(this.pc)
		},
		setUint8: function(a, b) {
      65535 >= a ? (this.memWriteMap.setUint8(a & 8191, b), 65532 === a ? this.frameReg[3] = b : 65533 === a ? this.frameReg[0] = b & this.romPageMask : 65534 === a ? this.frameReg[1] = b & this.romPageMask : 65535 === a && (this.frameReg[2] = b & this.romPageMask)) : JSSMS.Utils.console.error(JSSMS.Utils.toHex(a))
    },
		setUint16: function(a, b) {
      65532 > a ? this.memWriteMap.setUint16(a & 8191, b, LITTLE_ENDIAN) : 65532 === a ? (this.frameReg[3] = b & 255, this.frameReg[0] = b >> 8 & this.romPageMask) : 65533 === a ? (this.frameReg[0] = b & 255 & this.romPageMask, this.frameReg[1] = b >> 8 & this.romPageMask) : 65534 === a ? (this.frameReg[1] = b & 255 & this.romPageMask, this.frameReg[2] = b >> 8 & this.romPageMask) : JSSMS.Utils.console.error(JSSMS.Utils.toHex(a))
    },
		getUint8: function(a) {
      if (1024 > a) return this.rom[0].getUint8(a);
      if (16384 > a) return this.rom[this.frameReg[0]].getUint8(a);
      if (32768 > a) return this.rom[this.frameReg[1]].getUint8(a - 16384);
      if (49152 > a) return 8 === (this.frameReg[3] & 12) ? (this.useSRAM = !0, this.sram.getUint8(a - 32768)) : 12 === (this.frameReg[3] & 12) ? (this.useSRAM = !0, this.sram.getUint8(a - 16384)) : this.rom[this.frameReg[2]].getUint8(a - 32768);
      if (57344 > a) return this.memWriteMap.getUint8(a - 49152);
      if (65532 > a) return this.memWriteMap.getUint8(a - 57344);
      if (65532 === a) return this.frameReg[3];
      if (65533 === a) return this.frameReg[0];
      if (65534 === a) return this.frameReg[1];
      if (65535 === a) return this.frameReg[2];
      JSSMS.Utils.console.error(JSSMS.Utils.toHex(a));
      return 0
    },
		getUint16: function(a) {
      if (1024 > a) return this.rom[0].getUint16(a, LITTLE_ENDIAN);
      if (16384 > a) return this.rom[this.frameReg[0]].getUint16(a, LITTLE_ENDIAN);
      if (32768 > a) return this.rom[this.frameReg[1]].getUint16(a - 16384, LITTLE_ENDIAN);
      if (49152 > a) return 8 === (this.frameReg[3] & 12) ? (this.useSRAM = !0, this.sram[a -
        32768]) : 12 === (this.frameReg[3] & 12) ? (this.useSRAM = !0, this.sram[a - 16384]) : this.rom[this.frameReg[2]].getUint16(a - 32768, LITTLE_ENDIAN);
      if (57344 > a) return this.memWriteMap.getUint16(a - 49152, LITTLE_ENDIAN);
      if (65532 > a) return this.memWriteMap.getUint16(a - 57344, LITTLE_ENDIAN);
      if (65532 === a) return this.frameReg[3];
      if (65533 === a) return this.frameReg[0];
      if (65534 === a) return this.frameReg[1];
      if (65535 === a) return this.frameReg[2];
      JSSMS.Utils.console.error(JSSMS.Utils.toHex(a));
      return 0
    },
		getInt8: function(a) {
      var b = 0;
      if (1024 > a) b = this.rom[0].getInt8(a);
      else if (16384 > a) b = this.rom[this.frameReg[0]].getInt8(a);
      else if (32768 > a) b = this.rom[this.frameReg[1]].getInt8(a - 16384);
      else if (49152 > a) 8 === (this.frameReg[3] & 12) ? (this.useSRAM = !0, b = this.sram.getInt8(a - 32768)) : 12 === (this.frameReg[3] & 12) ? (this.useSRAM = !0, b = this.sram.getInt8(a - 16384)) : b = this.rom[this.frameReg[2]].getInt8(a - 32768);
      else if (57344 > a) b = this.memWriteMap.getInt8(a - 49152);
      else if (65532 > a) b = this.memWriteMap.getInt8(a - 57344);
      else {
        if (65532 === a) return this.frameReg[3];
        if (65533 === a) return this.frameReg[0];
        if (65534 === a) return this.frameReg[1];
        if (65535 === a) return this.frameReg[2];
        JSSMS.Utils.console.error(JSSMS.Utils.toHex(a))
      }
      return b + 1
    },
		hasUsedSRAM: function() {
			return this.useSRAM
		},
		setSRAM: function(a) {
			var b = a.length / PAGE_SIZE, d;
			for (d = 0; d < b; d++) JSSMS.Utils.copyArrayElements(a, d * PAGE_SIZE, this.sram[d], 0, PAGE_SIZE)
		},
		setStateMem: function(a) {
			this.frameReg = a
		},
		getState: function() {
			var a = Array(8);
			a[0] = this.pc | this.sp << 16;
			a[1] = (this.iff1 ? 1 : 0) | (this.iff2 ? 2 : 0) | (this.halt ? 4 : 0) | (this.EI_inst ?
				8 : 0) | (this.interruptLine ? 16 : 0);
			a[2] = this.a | this.a2 << 8 | this.f << 16 | this.f2 << 24;
			a[3] = this.getBC() | this.getDE() << 16;
			a[4] = this.getHL() | this.r << 16 | this.i << 24;
			a[5] = this.getIXHIXL() | this.getIYHIYL() << 16;
			this.exBC();
			this.exDE();
			this.exHL();
			a[6] = this.getBC() | this.getDE() << 16;
			a[7] = this.getHL() | this.im << 16 | this.interruptVector << 24;
			this.exBC();
			this.exDE();
			this.exHL();
			return a
		},
		setState: function(a) {
			var b = a[0];
			this.pc = b & 65535;
			this.sp = b >> 16 & 65535;
			b = a[1];
			this.iff1 = 0 !== (b & 1);
			this.iff2 = 0 !== (b & 2);
			this.halt = 0 !== (b & 4);
			this.EI_inst = 0 !== (b & 8);
			this.interruptLine = 0 !== (b & 16);
			b = a[2];
			this.a = b & 255;
			this.a2 = b >> 8 & 255;
			this.f = b >> 16 & 255;
			this.f2 = b >> 24 & 255;
			b = a[3];
			this.setBC(b & 65535);
			this.setDE(b >> 16 & 65535);
			b = a[4];
			this.setHL(b & 65535);
			this.r = b >> 16 & 255;
			this.i = b >> 24 & 255;
			b = a[5];
			this.setIXHIXL(b & 65535);
			this.setIYHIYL(b >> 16 & 65535);
			this.exBC();
			this.exDE();
			this.exHL();
			b = a[6];
			this.setBC(b & 65535);
			this.setDE(b >> 16 & 65535);
			b = a[7];
			this.setHL(b & 65535);
			this.im = b >> 16 & 255;
			this.interruptVector = b >> 24 & 255;
			this.exBC();
			this.exDE();
			this.exHL()
		}
	};
  
	var P1_KEY_UP = 1,
		P1_KEY_DOWN = 2,
		P1_KEY_LEFT = 4,
		P1_KEY_RIGHT = 8,
		P1_KEY_FIRE1 = 16,
		P1_KEY_FIRE2 = 32,
		P2_KEY_UP = 64,
		P2_KEY_DOWN = 128,
		P2_KEY_LEFT = 1,
		P2_KEY_RIGHT = 2,
		P2_KEY_FIRE1 = 4,
		P2_KEY_FIRE2 = 8,
		KEY_START = 64,
		GG_KEY_START = 128;
	JSSMS.Keyboard = function(a) {
		this.main = a;
		this.lightgunY = this.lightgunX = this.ggstart = this.controller2 = this.controller1 = 0;
		this.lightgunEnabled = this.lightgunClick = !1
	};
	JSSMS.Keyboard.prototype = {
		reset: function() {
			this.ggstart = this.controller2 = this.controller1 = 255;
			LIGHTGUN && (this.lightgunClick = !1);
			this.pause_button = !1
		}
	};

//extra sound impl start

JSSMS.SN76489 = function(sms) {
  this.main = sms
  var self = this
  var enabled = true
  var audioCtx = new window.AudioContext
  var soundchipFreq, sampleRate = audioCtx.sampleRate, sampleDecrement;
  var bufSize = 1024, audioProc = null

  this.init = function(clockSpeed) {
    soundchipFreq = clockSpeed / 16.0;
    //overall init
    sampleDecrement = soundchipFreq / sampleRate;
    if(audioProc) {
      audioProc.disconnect()
      audioProc = null
    }
    if(enabled) {
      console.log('Sound init')
      audioProc = audioCtx.createScriptProcessor(bufSize, 0, 1);
      audioProc.onaudioprocess = function(e) {
        generate(e.outputBuffer.getChannelData(0), bufSize)
      }
      audioProc.connect(audioCtx.destination)
    }
  }
  var register = [0, 0, 0, 0];
  var counter = [0, 0, 0, 0];
  var outputBit = [false, false, false, false];
  var volume = [0, 0, 0, 0];

  var volumeTable = [];
  var f = 1.0;
  var i;
  for(i = 0; i < 16; ++i) {
    volumeTable[i] = f / 4;  // Bakes in the per channel volume
    f *= Math.pow(10, -0.1);
  }
  volumeTable[15] = 0;

  function toneChannel(channel, out, length) {
      var i, reg = register[channel], vol = volume[channel];
      if(reg <= 1) {
          out.fill(vol);
          return;
      }
      for(i = 0; i < length; ++i) {
          counter[channel] -= sampleDecrement;
          if (counter[channel] < 0) {
              counter[channel] += reg;
              outputBit[channel] = !outputBit[channel];
          }
          out[i] += outputBit[channel] ? vol : -vol;
      }
  }

  var lfsr = 0;

  function shiftLfsrWhiteNoise() {
      var bit = (lfsr & 1) ^ ((lfsr & (1 << 3)) >> 3);
      lfsr = (lfsr >> 1) | (bit << 15);
  }

  function shiftLfsrPeriodicNoise() {
      lfsr >>= 1;
      if (lfsr === 0) lfsr = 1 << 15;
  }

  var shiftLfsr = shiftLfsrWhiteNoise;

  function noisePoked() {
      shiftLfsr = register[3] & 4 ? shiftLfsrWhiteNoise : shiftLfsrPeriodicNoise;
      lfsr = 1 << 15;
  }

  function addFor(channel) {
    return [0x10, 0x20, 0x40, register[channel - 1]][register[channel]&3]
  }

  function noiseChannel(channel, out, length) {
      var i, add = addFor(channel), vol = volume[channel];
      for(i = 0; i < length; ++i) {
          counter[channel] -= sampleDecrement;
          if (counter[channel] < 0) {
              counter[channel] += add;
              outputBit[channel] = !outputBit[channel];
              if (outputBit[channel]) shiftLfsr();
          }
          out[i] += (lfsr & 1) ? vol : -vol;
      }
  }

  function generate(out, length) {
    if(!enabled) return;
    out.fill(0.0)
    toneChannel(0, out, length);
    toneChannel(1, out, length);
    toneChannel(2, out, length);
    noiseChannel(3, out, length);
  }

  var latchedChannel = 0;

  function poke(value) {
    var latchData = !!(value & 0x80);
    if (latchData)
        latchedChannel = (value >> 5) & 3;
    if ((value & 0x90) === 0x90) {
        // Volume setting
        var newVolume = value & 0x0f;
        volume[latchedChannel] = volumeTable[newVolume];
    } else {
        // Data of some sort.
        if (latchedChannel === 3) {
            // For noise channel we always update the bottom bits of the register.
            register[latchedChannel] = value & 0x0f;
            noisePoked();
        } else if (latchData) {
            // Low 4 bits
            register[latchedChannel] = (register[latchedChannel] & ~0x0f) | (value & 0x0f);
        } else {
            // High bits
            register[latchedChannel] = (register[latchedChannel] & 0x0f) | ((value & 0x3f) << 4);
        }
    }
  }

  this.write = poke;
  this.reset = function () {
    for(var i = 0; i < 3; ++i)
      counter[i] = volume[i] = register[i] = 0;
    noisePoked();
  };
  this.enable = function (e) {
    enabled = e;
  };
  this.mute = function () {
    enabled = false;
  };
  this.unmute = function () {
    enabled = true;
  };
}

//extra sound impl end


	var NTSC = 0,
		PAL = 1,
		SMS_X_PIXELS = 342,
		SMS_Y_PIXELS_NTSC = 262,
		SMS_Y_PIXELS_PAL = 313,
		SMS_WIDTH = 256,
		SMS_HEIGHT = 192,
		GG_WIDTH = 160,
		GG_HEIGHT = 144,
		GG_X_OFFSET = 48,
		GG_Y_OFFSET = 24,
		STATUS_VINT = 128,
		STATUS_OVERFLOW = 64,
		STATUS_COLLISION = 32,
		STATUS_HINT = 4,
		BGT_LENGTH = 1792,
		SPRITES_PER_LINE = 8,
		SPRITE_COUNT = 0,
		SPRITE_X = 1,
		SPRITE_Y = 2,
		SPRITE_N = 3,
		TOTAL_TILES = 512,
		TILE_SIZE = 8;
	JSSMS.Vdp = function(a) {
		this.main = a;
		var b;
		this.videoMode = NTSC;
		this.VRAM = new Uint8Array(16384);
		this.CRAM = new Uint8Array(96);
		for (b = 0; 96 > b; b++) this.CRAM[b] = 255;
		this.vdpreg = new Uint8Array(16);
		this.status = 0;
		this.firstByte = !1;
		this.counter = this.line = this.readBuffer = this.operation = this.location = this.commandByte = 0;
		this.bgPriority = new Uint8Array(SMS_WIDTH);
		VDP_SPRITE_COLLISIONS && (this.spriteCol = new Uint8Array(SMS_WIDTH));
		this.vScrollLatch = this.bgt = 0;
		this.display = a.ui.canvasImageData.data;
		this.main_JAVA_R = new Uint8Array(64);
		this.main_JAVA_G = new Uint8Array(64);
		this.main_JAVA_B = new Uint8Array(64);
		this.GG_JAVA_R = new Uint8Array(256);
		this.GG_JAVA_G = new Uint8Array(256);
		this.GG_JAVA_B = new Uint8Array(16);
		this.sat = this.h_end = this.h_start = 0;
		this.isSatDirty = !1;
		this.lineSprites = Array(SMS_HEIGHT);
		for (b = 0; b < SMS_HEIGHT; b++) this.lineSprites[b] = new Uint8Array(1 + 3 * SPRITES_PER_LINE);
		this.tiles = Array(TOTAL_TILES);
		this.isTileDirty = new Uint8Array(TOTAL_TILES);
		this.maxDirty = this.minDirty = 0;
		this.createCachedImages();
		this.generateConvertedPals()
	};
	JSSMS.Vdp.prototype = {
		reset: function() {
			var a;
			this.firstByte = !0;
			for (a = this.operation = this.status = this.counter = this.location = 0; 16 > a; a++) this.vdpreg[a] = 0;
			this.vdpreg[2] = 14;
			this.vdpreg[5] = 126;
			this.vScrollLatch = 0;
			this.main.cpu.interruptLine = !1;
			this.isSatDirty = !0;
			this.minDirty = TOTAL_TILES;
			this.maxDirty = -1;
			for (a = 0; 16384 > a; a++) this.VRAM[a] = 0;
			for (a = 0; a < SMS_WIDTH * SMS_HEIGHT * 4; a += 4) this.display[a] = 0, this.display[a + 1] = 0, this.display[a + 2] = 0, this.display[a + 3] = 255
		},
		forceFullRedraw: function() {
			this.bgt = (this.vdpreg[2] & 14) << 10;
			this.minDirty = 0;
			this.maxDirty = TOTAL_TILES - 1;
			for (var a = 0; a < TOTAL_TILES; a++) this.isTileDirty[a] = 1;
			this.sat = (this.vdpreg[5] & -130) << 7;
			this.isSatDirty = !0
		},
		getVCount: function() {
			if (this.videoMode === NTSC) {
				if (218 < this.line) return this.line - 6
			} else if (242 < this.line) return this.line - 57;
			return this.line
		},
		controlRead: function() {
			this.firstByte = !0;
			var a = this.status;
			this.status = 0;
			this.main.cpu.interruptLine = !1;
			return a
		},
		controlWrite: function(a) {
			if (this.firstByte) this.firstByte = !1, this.commandByte = a, this.location = this.location & 16128 | a;
			else if (this.firstByte = !0, this.operation = a >> 6 & 3, this.location = this.commandByte | a << 8, 0 === this.operation) this.readBuffer = this.VRAM[this.location++ & 16383] & 255;
			else if (2 === this.operation) {
				a &= 15;
				switch (a) {
					case 0:
						break;
					case 1:
						0 !== (this.status & STATUS_VINT) && 0 !== (this.commandByte & 32) && (this.main.cpu.interruptLine = !0);
						(this.commandByte & 3) !== (this.vdpreg[a] & 3) && (this.isSatDirty = !0);
						break;
					case 2:
						this.bgt = (this.commandByte & 14) << 10;
						break;
					case 5:
						var b = this.sat;
						this.sat = (this.commandByte & -130) << 7;
						b !== this.sat && (this.isSatDirty = !0)
				}
				this.vdpreg[a] = this.commandByte
			}
		},
		dataRead: function() {
			this.firstByte = !0;
			var a = this.readBuffer;
			this.readBuffer = this.VRAM[this.location++ & 16383] & 255;
			return a
		},
		dataWrite: function(a) {
			var b;
			this.firstByte = !0;
			switch (this.operation) {
				case 0:
				case 1:
				case 2:
					b = this.location & 16383;
					if (a !== (this.VRAM[b] & 255)) {
						if (b >= this.sat && b < this.sat + 64 || b >= this.sat + 128 && b < this.sat +
							256) this.isSatDirty = !0;
						else {
							var d = b >> 5;
							this.isTileDirty[d] = 1;
							d < this.minDirty && (this.minDirty = d);
							d > this.maxDirty && (this.maxDirty = d)
						}
						this.VRAM[b] = a
					}
					break;
				case 3:
					this.main.is_sms ? (b = 3 * (this.location & 31), this.CRAM[b] = this.main_JAVA_R[a], this.CRAM[b + 1] = this.main_JAVA_G[a], this.CRAM[b + 2] = this.main_JAVA_B[a]) : (b = 3 * ((this.location & 63) >> 1), this.location & 1 ? this.CRAM[b + 2] = this.GG_JAVA_B[a] : (this.CRAM[b] = this.GG_JAVA_R[a], this.CRAM[b + 1] = this.GG_JAVA_G[a]))
			}
			ACCURATE && (this.readBuffer = a);
			this.location++
		},
		interrupts: function(a) {
			192 >= a ? (192 !== a || (this.status |= STATUS_VINT), 0 === this.counter ? (this.counter = this.vdpreg[10], this.status |= STATUS_HINT) : this.counter--, 0 !== (this.status & STATUS_HINT) && 0 !== (this.vdpreg[0] & 16) && (this.main.cpu.interruptLine = !0)) : (this.counter = this.vdpreg[10], 0 !== (this.status & STATUS_VINT) && 0 !== (this.vdpreg[1] & 32) && 224 > a && (this.main.cpu.interruptLine = !0), ACCURATE && a === this.main.no_of_scanlines - 1 && (this.vScrollLatch = this.vdpreg[9]))
		},
		setVBlankFlag: function() {
			this.status |= STATUS_VINT
		},
		drawLine: function(a) {
			var b, d;
			if (!this.main.is_gg || !(a < GG_Y_OFFSET || a >= GG_Y_OFFSET + GG_HEIGHT)) {
				if (VDP_SPRITE_COLLISIONS)
					for (b = 0; b < SMS_WIDTH; b++) this.spriteCol[b] = !1;
				if (0 !== (this.vdpreg[1] & 64)) {
					if (-1 !== this.maxDirty && this.decodeTiles(), this.drawBg(a), this.isSatDirty && this.decodeSat(), 0 !== this.lineSprites[a][SPRITE_COUNT] && this.drawSprite(a), this.main.is_sms && this.vdpreg[0] & 32)
						for (a = 4 * (a << 8), d = 3 * ((this.vdpreg[7] & 15) + 16), b = a; b < a + 32; b += 4) this.display[b] = this.CRAM[d], this.display[b + 1] = this.CRAM[d + 1], this.display[b +
							2] = this.CRAM[d + 2]
				} else this.drawBGColour(a)
			}
		},
		drawBg: function(a) {
			var b, d, f, c, g = this.vdpreg[8],
				h = ACCURATE ? this.vScrollLatch : this.vdpreg[9];
			16 > a && 0 !== (this.vdpreg[0] & 64) && (g = 0);
			var e = this.vdpreg[0] & 128,
				k = 32 - (g >> 3) + this.h_start,
				l = a + h >> 3;
			27 < l && (l -= 28);
			for (var h = (a + (h & 7) & 7) << 3, m = a << 8, p = this.h_start; p < this.h_end; p++) {
				b = this.bgt + ((k & 31) << 1) + (l << 6);
				var q = this.VRAM[b + 1],
					t = (q & 8) << 1,
					r = (p << 3) + (g & 7),
					u = 0 === (q & 4) ? h : 56 - h,
					v = this.tiles[(this.VRAM[b] & 255) + ((q & 1) << 8)];
				if (q & 2)
					for (b = 7; 0 <= b && r < SMS_WIDTH; b--, r++) d = v[b + u],
						f = 4 * (r + m), c = 3 * (d + t), this.bgPriority[r] = 0 !== (q & 16) && 0 !== d, r >= 8 * this.h_start && r < 8 * this.h_end && (this.display[f] = this.CRAM[c], this.display[f + 1] = this.CRAM[c + 1], this.display[f + 2] = this.CRAM[c + 2]);
				else
					for (b = 0; 8 > b && r < SMS_WIDTH; b++, r++) d = v[b + u], f = 4 * (r + m), c = 3 * (d + t), this.bgPriority[r] = 0 !== (q & 16) && 0 !== d, r >= 8 * this.h_start && r < 8 * this.h_end && (this.display[f] = this.CRAM[c], this.display[f + 1] = this.CRAM[c + 1], this.display[f + 2] = this.CRAM[c + 2]);
				k++;
				0 !== e && 23 === p && (l = a >> 3, h = (a & 7) << 3)
			}
		},
		drawSprite: function(a) {
			for (var b, d,
					f, c = 0, g = this.lineSprites[a], h = Math.min(SPRITES_PER_LINE, g[SPRITE_COUNT]), e = this.vdpreg[1] & 1, k = a << 8, l = 3 * h; c < h; c++) {
				var m = g[l--] | (this.vdpreg[6] & 4) << 6,
					p = g[l--],
					q = g[l--] - (this.vdpreg[0] & 8);
				b = a - p >> e;
				0 !== (this.vdpreg[1] & 2) && (m &= -2);
				m = this.tiles[m + ((b & 8) >> 3)];
				p = 0;
				0 > q && (p = -q, q = 0);
				var t = p + ((b & 7) << 3);
				if (e)
					for (; 8 > p && q < 8 * this.h_end; p++, q += 2) b = m[t++], q >= 8 * this.h_start && 0 !== b && !this.bgPriority[q] && (d = 4 * (q + k), f = 3 * (b + 16), this.display[d] = this.CRAM[f], this.display[d + 1] = this.CRAM[f + 1], this.display[d + 2] = this.CRAM[f +
						2], VDP_SPRITE_COLLISIONS && (this.spriteCol[q] ? this.status |= STATUS_COLLISION : this.spriteCol[q] = !0)), q + 1 >= 8 * this.h_start && 0 !== b && !this.bgPriority[q + 1] && (d = 4 * (q + k + 1), f = 3 * (b + 16), this.display[d] = this.CRAM[f], this.display[d + 1] = this.CRAM[f + 1], this.display[d + 2] = this.CRAM[f + 2], VDP_SPRITE_COLLISIONS && (this.spriteCol[q + 1] ? this.status |= STATUS_COLLISION : this.spriteCol[q + 1] = !0));
				else
					for (; 8 > p && q < 8 * this.h_end; p++, q++) b = m[t++], q >= 8 * this.h_start && 0 !== b && !this.bgPriority[q] && (d = 4 * (q + k), f = 3 * (b + 16), this.display[d] = this.CRAM[f],
						this.display[d + 1] = this.CRAM[f + 1], this.display[d + 2] = this.CRAM[f + 2], VDP_SPRITE_COLLISIONS && (this.spriteCol[q] ? this.status |= STATUS_COLLISION : this.spriteCol[q] = !0))
			}
			g[SPRITE_COUNT] >= SPRITES_PER_LINE && (this.status |= STATUS_OVERFLOW)
		},
		drawBGColour: function(a) {
			var b = 4 * (a << 8),
				d = 3 * ((this.vdpreg[7] & 15) + 16);
			for (a = b + 32 * this.h_start; a < b + 32 * this.h_end; a += 4) this.display[a] = this.CRAM[d], this.display[a + 1] = this.CRAM[d + 1], this.display[a + 2] = this.CRAM[d + 2]
		},
		decodeTiles: function() {
			for (var a = this.minDirty; a <= this.maxDirty; a++)
				if (this.isTileDirty[a]) {
					this.isTileDirty[a] =
						0;
					for (var b = this.tiles[a], d = 0, f = a << 5, c = 0; c < TILE_SIZE; c++)
						for (var g = this.VRAM[f++], h = this.VRAM[f++], e = this.VRAM[f++], k = this.VRAM[f++], l = 128; 0 !== l; l >>= 1) {
							var m = 0;
							0 !== (g & l) && (m |= 1);
							0 !== (h & l) && (m |= 2);
							0 !== (e & l) && (m |= 4);
							0 !== (k & l) && (m |= 8);
							b[d++] = m
						}
				} this.minDirty = TOTAL_TILES;
			this.maxDirty = -1
		},
		decodeSat: function() {
			this.isSatDirty = !1;
			for (var a = 0; a < this.lineSprites.length; a++) this.lineSprites[a][SPRITE_COUNT] = 0;
			a = 0 === (this.vdpreg[1] & 2) ? 8 : 16;
			1 === (this.vdpreg[1] & 1) && (a <<= 1);
			for (var b = 0; 64 > b; b++) {
				var d = this.VRAM[this.sat +
					b] & 255;
				if (208 === d) break;
				d++;
				240 < d && (d -= 256);
				for (var f = d; f < SMS_HEIGHT; f++)
					if (f - d < a) {
						var c = this.lineSprites[f];
						if (!c || c[SPRITE_COUNT] >= SPRITES_PER_LINE) break;
						var g = 3 * c[SPRITE_COUNT] + SPRITE_X,
							h = this.sat + (b << 1) + 128;
						c[g++] = this.VRAM[h++] & 255;
						c[g++] = d;
						c[g++] = this.VRAM[h] & 255;
						c[SPRITE_COUNT]++
					}
			}
		},
		createCachedImages: function() {
			for (var a = 0; a < TOTAL_TILES; a++) this.tiles[a] = new Uint8Array(TILE_SIZE * TILE_SIZE)
		},
		generateConvertedPals: function() {
			var a, b, d, f;
			for (a = 0; 64 > a; a++) b = a & 3, d = a >> 2 & 3, f = a >> 4 & 3, this.main_JAVA_R[a] =
				85 * b & 255, this.main_JAVA_G[a] = 85 * d & 255, this.main_JAVA_B[a] = 85 * f & 255;
			for (a = 0; 256 > a; a++) d = a & 15, f = a >> 4 & 15, this.GG_JAVA_R[a] = (d << 4 | d) & 255, this.GG_JAVA_G[a] = (f << 4 | f) & 255;
			for (a = 0; 16 > a; a++) this.GG_JAVA_B[a] = (a << 4 | a) & 255
		},
		getState: function() {
			var a = Array(51);
			a[0] = this.videoMode | this.status << 8 | (this.firstByte ? 65536 : 0) | this.commandByte << 24;
			a[1] = this.location | this.operation << 16 | this.readBuffer << 24;
			a[2] = this.counter | this.vScrollLatch << 8 | this.line << 16;
			JSSMS.Utils.copyArrayElements(this.vdpreg, 0, a, 3, 16);
			JSSMS.Utils.copyArrayElements(this.CRAM, 0, a, 19, 96);
			return a
		},
		setState: function(a) {
			var b = a[0];
			this.videoMode = b & 255;
			this.status = b >> 8 & 255;
			this.firstByte = 0 !== (b >> 16 & 255);
			this.commandByte = b >> 24 & 255;
			b = a[1];
			this.location = b & 65535;
			this.operation = b >> 16 & 255;
			this.readBuffer = b >> 24 & 255;
			b = a[2];
			this.counter = b & 255;
			this.vScrollLatch = b >> 8 & 255;
			this.line = b >> 16 & 65535;
			JSSMS.Utils.copyArrayElements(a, 3, this.vdpreg, 0, 16);
			JSSMS.Utils.copyArrayElements(a, 19, this.CRAM, 0, 96);
			this.forceFullRedraw()
		}
	};

	JSSMS.DummyUI = function(a) {
		this.main = a;
		this.reset = function() {};
		this.updateStatus = function() {};
		this.writeFrame = function() {};
		this.updateDisassembly = function() {};
		this.canvasImageData = {
			data: []
		}
	};

	var IO_TR_DIRECTION = 0,
		IO_TH_DIRECTION = 1,
		IO_TR_OUTPUT = 2,
		IO_TH_OUTPUT = 3,
		IO_TH_INPUT = 4,
		PORT_A = 0,
		PORT_B = 5;
	JSSMS.Ports = function(a) {
		this.main = a;
		this.vdp = a.vdp;
		this.psg = a.psg;
		this.keyboard = a.keyboard;
		this.europe = 64;
		this.hCounter = 0;
		this.ioPorts = []
	};

	JSSMS.Ports.prototype = {
		reset: function() {
			LIGHTGUN ? (this.ioPorts = Array(10), this.ioPorts[PORT_A + IO_TH_INPUT] = 1, this.ioPorts[PORT_B + IO_TH_INPUT] = 1) : this.ioPorts = Array(2)
		},
		out: function(a, b) {
			if (!(this.main.is_gg && 7 > a)) switch (a & 193) {
				case 1:
					LIGHTGUN ? (this.oldTH = 0 !== this.getTH(PORT_A) || 0 !== this.getTH(PORT_B), this.writePort(PORT_A, b), this.writePort(PORT_B, b >> 2), this.oldTH || 0 === this.getTH(PORT_A) && 0 === this.getTH(PORT_B) || (this.hCounter = this.getHCount())) : (this.ioPorts[0] = (b & 32) << 1, this.ioPorts[1] = b & 128, 0 ===
						this.europe && (this.ioPorts[0] = ~this.ioPorts[0], this.ioPorts[1] = ~this.ioPorts[1]));
					break;
				case 128:
					this.vdp.dataWrite(b);
					break;
				case 129:
					this.vdp.controlWrite(b);
					break;
				case 64:
				case 65:
				  this.psg.write(b)
			}
		},
		in_: function(a) {
			if (this.main.is_gg && 7 > a) switch (a) {
				case 0:
					return this.keyboard.ggstart & 191 | this.europe;
				case 1:
				case 2:
				case 3:
				case 4:
				case 5:
					return 0;
				case 6:
					return 255
			}
			switch (a & 193) {
				case 64:
					return this.vdp.getVCount();
				case 65:
					return this.hCounter;
				case 128:
					return this.vdp.dataRead();
				case 129:
					return this.vdp.controlRead();
				case 192:
					return this.keyboard.controller1;
				case 193:
					return LIGHTGUN ? (this.keyboard.lightgunClick && this.lightPhaserSync(), this.keyboard.controller2 & 63 | (0 !== this.getTH(PORT_A) ? 64 : 0) | (0 !== this.getTH(PORT_B) ? 128 : 0)) : this.keyboard.controller2 & 63 | this.Ports[0] | this.ioPorts[1]
			}
			return 255
		},
		writePort: function(a, b) {
			this.ioPorts[a + IO_TR_DIRECTION] = b & 1;
			this.ioPorts[a + IO_TH_DIRECTION] = b & 2;
			this.ioPorts[a + IO_TR_OUTPUT] = b & 16;
			this.ioPorts[a + IO_TH_OUTPUT] = 0 === this.europe ? ~b & 32 : b & 32
		},
		getTH: function(a) {
			return 0 === this.ioPorts[a + IO_TH_DIRECTION] ? this.ioPorts[a + IO_TH_OUTPUT] : this.ioPorts[a + IO_TH_INPUT]
		},
		setTH: function(a, b) {
			this.ioPorts[a + IO_TH_DIRECTION] = 1;
			this.ioPorts[a + IO_TH_INPUT] = b ? 1 : 0
		},
		getHCount: function() {
			var a = Math.round(this.main.cpu.getCycle() * SMS_X_PIXELS / this.main.cyclesPerLine) - 8 >> 1;
			147 < a && (a += 85);
			return a & 255
		},
		X_RANGE: 48,
		Y_RANGE: 4,
		lightPhaserSync: function() {
			var a = this.getTH(PORT_A),
				b = this.getHCount(),
				d = this.keyboard.lightgunX - (b << 1),
				f = this.keyboard.lightgunY -
				this.vdp.line;
			f > -this.Y_RANGE && f < this.Y_RANGE && d > -this.X_RANGE && d < this.X_RANGE ? (this.setTH(PORT_A, !1), a !== this.getTH(PORT_A) && (this.hCounter = 20 + (this.keyboard.lightgunX >> 1))) : (this.setTH(PORT_A, !0), a !== this.getTH(PORT_A) && (this.hCounter = b))
		},
		setDomestic: function(a) {
			this.europe = a ? 64 : 0
		},
		isDomestic: function() {
			return 0 !== this.europe
		}
	};

	window.JSSMS = JSSMS;
})(window);
