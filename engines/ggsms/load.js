window.LoadEngine = function(engineCfg, titleElem, canvasElem, gameData, gameTitle, gameFile) {
  var baseName = gameTitle.split('/').pop(), romBuffer = '', i, l = gameData.length
  for(i=0;i<l;i++)
    romBuffer += String.fromCharCode(gameData[i])
  titleElem.textContent = baseName
  var mappingToMask = {
    'Up': 0x01,
    'Down': 0x02,
    'Left': 0x04,
    'Right': 0x08,
    'Action1': 0x10,
    'Action2': 0x20,
    'GGStart': 0x80,
    'Start': 0x40,
    'Pause': 0x40
  }, mapping = {};
  for(var k in engineCfg.keys)
    if(!k.startsWith('sup_'))
      mapping[engineCfg.keys[k]] = k

  function GGKeyDown(mv) {
    if(mv in mappingToMask) {
      if(window.SMS.is_gg && mv === 'Start') {
        window.SMS.keyboard.ggstart &= ~mappingToMask['GGStart']
      } else
        window.SMS.keyboard.controller1 &= ~mappingToMask[mv]
    }
  }

  function GGKeyUp(mv) {
    if(mv in mappingToMask) {
      if(window.SMS.is_gg && mv === 'Start') {
        window.SMS.keyboard.ggstart |= mappingToMask['GGStart']
      } else
      window.SMS.keyboard.controller1 |= mappingToMask[mv]
    }
  }

  //load the engine script
  var s = document.createElement('script')
  s.src = window.engineRootDir + '/jssms-opti.js'
  s.onload = function() {
    var machinePaused = true
    //setup keys
    window.addEventListener('keydown', function(e) {
      if(e.key in mapping) {
        e.preventDefault()
        GGKeyDown(mapping[e.key])
      }
      else if(e.key === engineCfg.keys.sup_Pause) {
        if(!machinePaused) {
          machinePaused = true
          for(var key in mapping)
            GGKeyUp(mapping[key])
          SMS.psg.mute()
          SMS.stop()
        }
        else {
          machinePaused = false
          SMS.psg.unmute()
          SMS.start()
        }
      }
    })
    window.addEventListener('keyup', function(e) {
      if(e.key in mapping) {
        e.preventDefault()
        GGKeyUp(mapping[e.key])
      }
    })
    //start the machine
    window.SMS = new JSSMS({ui: function(sms) {
      this.main = sms
      this.reset = function() {};
      this.updateStatus = function(s) {
        this.xoffset = 0
        this.yoffset = 0
        if(sms.is_gg) {
          engineCfg.screen.width = canvasElem.width = 160
          engineCfg.screen.height = canvasElem.height = 144
          this.xoffset = -48
          this.yoffset = -24
          canvasElem.style.height = Math.round(canvasElem.clientWidth * engineCfg.screen.height / engineCfg.screen.width)+'px'
          canvasElem.style.top = 'calc(50% - ' + (canvasElem.clientHeight>>1) + 'px)'
        }
        else {
          canvasElem.width =  256
          canvasElem.height = 192
        }
      };
      this.writeFrame = function() {
        this.canvasContext.putImageData(this.canvasImageData, this.xoffset, this.yoffset);
      };
      this.updateDisassembly = function() {};
      this.canvasContext = canvasElem.getContext('2d', {alpha: false});
      this.canvasContext['mozImageSmoothingEnabled'] = false;
      this.canvasContext['imageSmoothingEnabled'] = false;
      this.canvasImageData = this.canvasContext.getImageData(0,0,256,192);
    }});
    console.log('Loading')
    window.SMS.stop();
    window.SMS.readRomDirectly(romBuffer, baseName);
    var soundOn = window.confirm('Run with sound?')
    window.SMS.psg.enable(soundOn);
    window.SMS.reset();
    window.SMS.vdp.forceFullRedraw();
    console.log('ROM loaded')
    //init engine end
    window.SMS.start()
    machinePaused = false
    //update canvas
    canvasElem.style.height = Math.round(canvasElem.clientWidth * engineCfg.screen.height / engineCfg.screen.width)+'px'
    canvasElem.style.top = 'calc(50% - ' + (canvasElem.clientHeight>>1) + 'px)'
  }
  document.body.appendChild(s)

}
