# MobiArch: a multiplatform emulator framework for KaiOS

Picture something like RetroArch but for a feature phone. This is the goal of the MobiArch project: to integrate multiple retro platform emulators in a single KaiOS app.

MobiArch started out as [MobiPICO](https://gitlab.com/suborg/mobipico) (which is still available there for historical reasons) but has been extended to facilitate porting of any JS-based emulator engines to KaiOS.

## Usage and configuration

Start the app and pick a game file using the system `pick` activity by pressing D-Pad center or Call key. That's it!

The configuration file (`config.js`), however, is where most of the magic happens. All configuration is done using the `emuConfig` object.

### Project name

You can change the project name in the `emuconfig.global.name` property.

### Extension mapping

Every file extension that MobiArch understands must be registered in the `emuConfig.extensions` object.  The value is an object with two fields - `engine` (the actual engine ID, see below) and `loadAs`. If the `loadAs` field is set to `url`, this means that the underlying engine expects to load the file via `XMLHttpRequest` and it must be passed as a URL (don't worry, MobiArch does the conversion for you). If the `loadAs` field is set to anything else, for now this means that the file is loaded by the MobiArch as a data array and passed to the engine this way.  

### Engine definitions: adding your own or modifying the existing ones

All engines MobiArch supports are defined in the `emuConfig.engines` object. Every engine must have a unique ID that is the key in this object and the name of the root directory in the `engines` directory of the project at the same time. For instance, for CHIP-8, the ID is `chip8`, so it means `the emuConfig.engines.chip8` object and `/engines/chip8` root directory.

In the `/engines/[engine_id]` directory, there may be as many actual engine files as necessary but there **must** be a `load.js` file with a global `LoadEngine` function defined as follows:

```js
window.LoadEngine = function(engineCfg, titleElem, canvasElem, gameData, gameTitle, gameFile) {
  //... code that prepares the engine, does all event setup and dynamically loads the other files
}
```

The parameters of the function are:

- `engineCfg` - configuration object of the engine (the `emuConfig.[engine_id]` section);
- `titleElem` - DOM element where the game title is rendered;
- `canvasElem` - DOM element with the canvas where the game graphics is rendered;
- `gameData` - data array with the game ROM (empty in case of loading as a URL);
- `gameTitle` - game title that defaults to the full file name;
- `gameFile` - actual game file URL (unused in case of loading as file data).

Additionally, useful variables such as `window.engineId` and `window.engineRootDir` are available to the `LoadEngine` function.

### Screen size

Every engine has its own actual screen size. For MobiArch to adjust to it, it must be told about the dimensions by setting `width` and `height` values in the `emuConfig.engines.[engine_id].screen` object.

### Controls

You can adjust the controls by modifying the `emuConfig.engines.[engine_id].keys` object in the config file. Object format, available keys and their functions fully depend on the engine, for instance, for PICO-8 they are:

- D-pad directions (`Left`, `Right`, `Up`, `Down`),
- `Action1` ('O' in PICO-8),
- `Action2` ('X' in PICO-8),
- `Pause` (Menu in PICO-8),
- `SwapActionBtns` (the key to swap `Action1` and `Action2` buttons for some games).

Regardless of the engine, the End/Backspace key must be reserved for closing the game and/or the emulator if confirmed so.

## Currently supported platforms, engines and file extensions

Fully supported:

- CHIP-8 (`.ch8`, `.c8`): [DALE-8](https://gitlab.com/suborg/dale-8) v0.0.5, by Luxferre
- PICO-8 (`.p8`, `.p8.png`): official [PICO-8](https://www.lexaloffle.com/pico-8.php) v0.24 web player, by Lexaloffle Games

Partial or buggy support:

- Nintendo Game Boy and GameBoyColor (`.gb`, `.gbc`): jsGB/GameBoyCore by Grant Galitz with optimizations by Luxferre (no sound on some KaiOS phone models)
- Sega Game Gear and Master System (`.gg`, `.sms`): jsSMS by gmarty with optimizations by Luxferre (severe sound lags as of now, incomplete Master System support)

## Credits

The emulator engine files are distributed under the terms of their respective authors.

All other parts of this project, including but not limited to `load.js` files in the engine directories, are created by Luxferre and released into public domain (see `UNLICENSE`).
