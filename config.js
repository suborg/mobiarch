window.emuConfig = {
  global: {
    name: 'MobiArch'
  },
  extensions: { //map extension to engine and loading method
    '.c8': {engine: 'chip8', loadAs: 'data'},
    '.ch8': {engine: 'chip8', loadAs: 'data'},
    '.p8': {engine: 'pico8', loadAs: 'data'},
    '.p8.png': {engine: 'pico8', loadAs: 'url'},
    '.gb': {engine: 'gb', loadAs: 'data'},
    '.gbc': {engine: 'gb', loadAs: 'data'},
    '.gg': {engine: 'ggsms', loadAs: 'data'},
    '.sms': {engine: 'ggsms', loadAs: 'data'}
  },
  engines: { //engine feature definitions
    chip8: {
      screen: {
        width: 256,
        height: 128
      },
      keys: ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '*', '#', 'Call', 'SoftLeft', 'SoftRight', 'Enter']
    },
    pico8: {
      screen: {
        width: 128,
        height: 128
      },
      keys: {
        Left: 'ArrowLeft',
        Right: 'ArrowRight',
        Up: 'ArrowUp',
        Down: 'ArrowDown',
        Action1: '8', //O
        Action2: '9', //X
        Pause: 'Call',
        SwapActionBtns: '#'
      }
    },
    gb: {
      screen: {
        width: 160,
        height: 144
      },
      keys: {
        Left: 'ArrowLeft',
        Right: 'ArrowRight',
        Up: 'ArrowUp',
        Down: 'ArrowDown',
        A: '9',
        B: '8',
        Start: 'SoftRight',
        Select: 'SoftLeft',
        sup_Pause: 'Call'
      }
    },
    ggsms: {
      screen: {
        width: 256,
        height: 192
      },
      keys: {
        Left: 'ArrowLeft',
        Right: 'ArrowRight',
        Up: 'ArrowUp',
        Down: 'ArrowDown',
        Action1: '8',
        Action2: '9',
        Start: 'SoftRight',
        Pause: '*', 
        sup_Pause: 'Call'
      }
    }
  }
}
